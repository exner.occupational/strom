# Virtual Environments
#alias vst=". ~/virtualenvs/strom/bin/activate && cd ~/git/strom/"
alias vst=". ~/virtualenvs/strom/bin/activate"
alias vxx='deactivate && rm -r ~/virtualenvs/strom && mkdir ~/virtualenvs/strom && python3 -m venv ~/virtualenvs/strom && . ~/virtualenvs/strom/bin/activate && pip install pip-tools'
#alias vha=". ~/virtualenvs/heating-analysis/bin/activate && cd ~/git/heating-analysis/"
#alias vse=". ~/virtualenvs/service/bin/activate"

# Development
alias l='python -m licenseheaders -E py html -x setup.py version.py -n strom -y 2024 -t ~/git/strom/.gpl-v3.tmpl -o "Alexander Exner" -u "https://gitlab.com/exner.occupational/strom"'
alias pi='pip install --editable .[test]'
alias pc='pip-compile -r requirements.in'

# Test
alias s="python -m strom.service.app"

# Remote Deployment
alias u='ssh pi@strom "cd /home/pi/git/strom && git pull"'
alias p='ssh pi@strom ". /home/pi/virtualenvs/strom/bin/activate && cd /home/pi/git/strom && pip install --editable .[test]"'
alias i='ssh pi@strom ". /home/pi/virtualenvs/strom/bin/activate && cd /home/pi/git/strom && python setup.py bdist_wheel && sudo systemctl stop strom && . /home/pi/virtualenvs/service/bin/activate && pip install /home/pi/git/strom/dist/\$(ls /home/pi/git/strom/dist |sort |tail -1) && sudo systemctl start strom"'
alias jst='ssh pi@strom "journalctl -u strom -f"'

# Local Deployment
#alias bld="python setup.py bdist_wheel"
#alias ins='pip install ~/git/strom/dist/strom-$(python ~/git/strom/strom/version.py)-py3-none-any.whl'
#alias sta="sudo systemctl start strom"
#alias sto="sudo systemctl stop strom"
#alias i='vse ; pip install dist/$(ls dist |sort |tail -1) ; vst'

# Misc
# list git controlled files (within current directory) as tree
alias tg='tree --gitignore --prune --noreport --sort=version'
# list python files (within project directory) as tree, omit pytest files
alias tp='tree -P "*.py" -I "test*" --prune --noreport --sort=version ~/git/strom/strom'
