#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import pytest
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from strom.service.db import DBDomain, DB
from strom.util.action import Main, Avail
from strom.util.epoch import vienna_now, get_epoch_from_datetime
from strom.util.const import VIENNA


def test_1(db):
    count = db.truncate(relativedelta())
    #print(f"count: {count}")
    db.close()

def test_2(db):
    """
    Set and retrieve two values
    """
    vnow = vienna_now()
    epoch = get_epoch_from_datetime(vnow, ms=False)
    #db = DB()
    db.put(DBDomain.AVAIL, epoch - 1, Avail.ON)
    db.put(DBDomain.AVAIL, epoch - 2, Avail.OFF)
    storage = db.get(DBDomain.AVAIL)
    assert storage[epoch - 1] == Avail.ON
    assert storage[epoch - 2] == Avail.OFF
    db.close()

def test_3(db):
    """
    expected: delete two records from previous test
    """
    count = db.truncate(relativedelta())
    assert count == 2
    db.close()

def test_4(db):
    #vnow = vienna_now()
    #epoch = get_epoch_from_datetime(vnow, ms=False)
    #db = DB()

    #XDAYS = relativedelta(days=-40, hour=0, minute=0, second=0, microsecond=0)
    vnow = vienna_now()
    #dt = vnow + XDAYS
    epoch = get_epoch_from_datetime(vnow, ms=False)

    db.put(DBDomain.AVAIL, epoch - 1, Avail.ON)
    db.put(DBDomain.AVAIL, epoch, Avail.ON)
    db.put(DBDomain.AVAIL, epoch + 1, Avail.OFF)
    count = db.truncate(relativedelta(), epoch)
    assert count == 1
    storage = db.get(DBDomain.AVAIL)
    assert storage[epoch] == Avail.ON
    assert storage[epoch + 1] == Avail.OFF
    #assert db.get(DB._encode_key(DBDomain.AVAIL, epoch)) == Avail.ON
    #assert db.get(DB._encode_key(DBDomain.AVAIL, epoch + 1)) == Avail.OFF
    db.close()


def test_exc_1(db):
    """
    Put wrong epoch (including miliseconds) to database
    """
    vnow = vienna_now()
    epoch = get_epoch_from_datetime(vnow, ms=True)
    with pytest.raises(Exception):
        db.put(DBDomain.AVAIL, epoch, True)
    db.close()

def test_get_key_less_or_equal_1(db):
    db.put(DBDomain.AVAIL, 1700000000, Avail.ON)
    db.put(DBDomain.AVAIL, 1700000010, Avail.ON)
    db.put(DBDomain.AVAIL, 1700000020, Avail.OFF)
    db.put(DBDomain.AVAIL, 1700000030, Avail.ON)
    db.put(DBDomain.AVAIL, 1700000040, Avail.ON)
    assert db.get_key_less_or_equal(DBDomain.AVAIL, 1700000010) == DB._encode_key(DBDomain.AVAIL, 1700000010)
    assert db.get_key_less_or_equal(DBDomain.AVAIL, 1700000015) == DB._encode_key(DBDomain.AVAIL, 1700000010)
    assert db.get_key_less_or_equal(DBDomain.AVAIL, 1700000029) == DB._encode_key(DBDomain.AVAIL, 1700000020)
    assert db.get_key_less_or_equal(DBDomain.AVAIL, 1700000030) == DB._encode_key(DBDomain.AVAIL, 1700000030)
    assert db.get_key_less_or_equal(DBDomain.AVAIL, 1700000031) == DB._encode_key(DBDomain.AVAIL, 1700000030)
    assert db.get_key_less_or_equal(DBDomain.AVAIL, 1700000050) == DB._encode_key(DBDomain.AVAIL, 1700000040)
    db.close()

def test_get_intervals_strict_1(db):
    data = [(datetime(2023, 10, 10, 12, 0, tzinfo=VIENNA), Main.OFF),
            (datetime(2023, 10, 10, 12, 1, tzinfo=VIENNA), Main.OFF),
            (datetime(2023, 10, 10, 12, 3, tzinfo=VIENNA), Main.H1),
            (datetime(2023, 10, 10, 12, 4, tzinfo=VIENNA), Main.H1),
            (datetime(2023, 10, 10, 12, 5, tzinfo=VIENNA), Main.NS),
            (datetime(2023, 10, 10, 12, 6, tzinfo=VIENNA), Main.NS),
            # missing
            (datetime(2023, 10, 10, 12, 8, tzinfo=VIENNA), Main.OFF),
            (datetime(2023, 10, 10, 12, 9, tzinfo=VIENNA), Main.FAULTY),
            # missing
            (datetime(2023, 10, 10, 13, 1, tzinfo=VIENNA), Main.OFF),
            (datetime(2023, 10, 10, 13, 2, tzinfo=VIENNA), Main.OFF),
            (datetime(2023, 10, 10, 13, 3, tzinfo=VIENNA), Main.FAULTY),
            (datetime(2023, 10, 10, 13, 4, tzinfo=VIENNA), Main.OFF),
            # missing
            (datetime(2023, 10, 10, 13, 6, tzinfo=VIENNA), Main.OFF)
           ]

    for d in data:
        #db.put(DBDomain.MAIN, db._encode_epoch(get_epoch_from_datetime(d[0], ms=False)), d[1])
        db.put(DBDomain.MAIN, get_epoch_from_datetime(d[0], ms=False), d[1])

    result = db.get_intervals_strict(DBDomain.MAIN, Main.OFF, datetime(2023, 10, 10, 12, 0, tzinfo=VIENNA), datetime(2023, 10, 10, 13, 6, tzinfo=VIENNA))
    assert result == [(datetime(2023, 10, 10, 12, 0, tzinfo=VIENNA), timedelta(minutes=2)),
                      (datetime(2023, 10, 10, 12, 8, tzinfo=VIENNA), timedelta(minutes=1)),
                      (datetime(2023, 10, 10, 13, 1, tzinfo=VIENNA), timedelta(minutes=2)),
                      (datetime(2023, 10, 10, 13, 4, tzinfo=VIENNA), timedelta(minutes=1))
                     ]
    result = db.get_intervals_strict(DBDomain.MAIN, Main.OFF, datetime(2023, 10, 10, 12, 0, tzinfo=VIENNA), datetime(2023, 10, 10, 13, 7, tzinfo=VIENNA))
    assert result == [(datetime(2023, 10, 10, 12, 0, tzinfo=VIENNA), timedelta(minutes=2)),
                      (datetime(2023, 10, 10, 12, 8, tzinfo=VIENNA), timedelta(minutes=1)),
                      (datetime(2023, 10, 10, 13, 1, tzinfo=VIENNA), timedelta(minutes=2)),
                      (datetime(2023, 10, 10, 13, 4, tzinfo=VIENNA), timedelta(minutes=1)),
                      (datetime(2023, 10, 10, 13, 6, tzinfo=VIENNA), timedelta(minutes=1))
                     ]
    db.close()

def test_get_generator_1(db):
    db.put(DBDomain.PRICE, 1700001000, 123.4)
    db.put(DBDomain.PRICE, 1700002000, 23.4)
    db.put(DBDomain.PRICE, 1700003000, 3.4)
    gen = db.get_generator(DBDomain.PRICE, 1700001500, 1700002500)
    for x in gen:
        assert x == (1700002000, 23.4)
    db.close()
