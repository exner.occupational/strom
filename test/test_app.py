#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from datetime import datetime
from strom.util.const import VIENNA #, UTC
from strom.service.app import RecalcTracker


def test_recalc_schedule_1():
    dt = datetime(2023, 4, 3, 14, 18, 32, 324, tzinfo=VIENNA)
    recalc_schedule = RecalcTracker()
    assert recalc_schedule.needed(dt) == True
    dt = datetime(2023, 4, 3, 14, 19, 5, 6, tzinfo=VIENNA)
    recalc_schedule.done(dt)
    assert recalc_schedule.needed(dt) == False
    dt = datetime(2023, 4, 4, 10, 0, 0, 0, tzinfo=VIENNA)
    assert recalc_schedule.needed(dt) == False
    dt = datetime(2023, 4, 4, 15, 0, 0, 0, tzinfo=VIENNA)
    assert recalc_schedule.needed(dt) == True

