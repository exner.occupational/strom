#
# Copyright (c) 2024 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from datetime import date, datetime, timedelta
from strom.util.const import VIENNA, LATITUDE, LONGITUDE
from strom.util.sunrise import Sun
from strom.util.sun import calc_sunrise_sunset_a, calc_sunrise_sunset_b
#from ..util import epoch
#from ..util.epoch import get_epoch_from_datetime, get_epoch_from_date, get_datetime_from_epoch, get_batches
#from ..util.const import VIENNA, UTC
#from datetime import datetime, date
#import pytest



# Test-Daten Sonnenaufgang
# date, sunrise, sunset

sunrises = [
    (date(2045, 1, 19), datetime(2045, 1, 19, 7, 37, tzinfo=VIENNA), datetime(2045, 1, 19, 16, 35, tzinfo=VIENNA)),
    (date(2024, 2, 29), datetime(2024, 2, 29, 6, 37, tzinfo=VIENNA), datetime(2024, 2, 29, 17, 38, tzinfo=VIENNA)),
    (date(2024, 3, 9), datetime(2024, 3, 9, 6, 20, tzinfo=VIENNA), datetime(2024, 3, 9, 17, 52, tzinfo=VIENNA)),
    (date(2024, 3, 30), datetime(2024, 3, 30, 5, 36, tzinfo=VIENNA), datetime(2024, 3, 30, 18, 23, tzinfo=VIENNA)),
    (date(2024, 3, 31), datetime(2024, 3, 31, 6, 34, tzinfo=VIENNA), datetime(2024, 3, 31, 19, 25, tzinfo=VIENNA)),
    (date(2015, 8, 22), datetime(2015, 8, 22, 5, 58, tzinfo=VIENNA), datetime(2015, 8, 22, 19, 57, tzinfo=VIENNA))]



def compare_datetime_with_tolerance(dt1, dt2, tolerance_minutes: int = 5):
    """
    Compares two datetime objects with tolerance in minutes, considering tzinfo.

    Args:
        dt1 (datetime): First datetime object.
        dt2 (datetime): Second datetime object.
        tolerance_minutes (int): Tolerance in minutes for comparison.

    Returns:
        bool: True if the difference between the datetimes is within the tolerance, False otherwise.
    """

    # Convert tolerance to timedelta
    tolerance = timedelta(minutes=tolerance_minutes)

    return abs(dt1 - dt2) <= tolerance


def test_sunrise_sunset_1():
    for (my_date, my_sunrise, my_sunset) in sunrises:
        #my_dt = datetime(my_date.year, my_date.month, my_date.day, tzinfo=VIENNA)
        sun = Sun(my_date, LATITUDE, LONGITUDE)
        sunrise = sun.sunrise()
        assert compare_datetime_with_tolerance(sunrise, my_sunrise)
        sunset = sun.sunset()
        assert compare_datetime_with_tolerance(sunset, my_sunset)

#def test_sunrise_2():
#    for (my_date, my_sunrise) in sunrises:
#        (sunrise, sunset) = calc_sunrise_sunset_a(my_date)
#        assert compare_datetime_with_tolerance(sunrise, my_sunrise)
