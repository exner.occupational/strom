#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from strom.util.calc import calc_distribution
from strom.model.market import SimpleMarketDataModel, SimpleMarketDataList
#from ..util import epoch
#from ..util.epoch import get_epoch_from_datetime, get_epoch_from_date, get_datetime_from_epoch, get_batches
#from ..util.const import VIENNA, UTC
#from datetime import datetime, date
#import pytest




from datetime import datetime, timedelta
dt = datetime(2023, 9, 12, 0, 0, 0, 0)


pl1 = [
    (0,33.14),
    (1,25.79),
    (2,20.18),
    (3,16.12),
    (4,21.9),
    (5,32.96),
    (6,45),
    (7,92.05),
    (8,112.89),
    (9,114.59),
    (10,104.74),
    (11,116.94),
    (12,138.76),
    (13,124.91),
    (14,139.08),
    (15,143.32),
    (16,147.58),
    (17,158.69),
    (18,164.69),
    (19,157.42),
    (20,147.56),
    (21,128.8),
    (22,107.84),
    (23,88.17)]

l1 = [SimpleMarketDataModel(start_dt=dt + timedelta(hours=x[0]), marketprice=x[1]) for x in pl1]
smdl1 = SimpleMarketDataList.model_validate(l1)
print("smdl1")
print(smdl1)


pl2 = [
    (3,27.2),
    (4,27.9),
    (23,40.82),
    (11,41.59),
    (9,41.63),
    (13,38.73),
    (6,38.77),
    (17,38.87),
    (7,42.84),
    (21,45.32),
    (18,46),
    (22,46),
    (10,42.14),
    (2,28.38),
    (5,30.19),
    (1,30.91),
    (16,34.22),
    (0,34.53),
    (15,35.77),
    (14,36.39),
    (8,46.42),
    (20,47.91),
    (12,49.81),
    (19,52.51)]

l2 = [SimpleMarketDataModel(start_dt=dt + timedelta(hours=x[0]), marketprice=x[1]) for x in pl2]
smdl2 = SimpleMarketDataList.model_validate(l2)
print("smdl2")
print(smdl2)

def ints_to_dts(li):
    return [dt + timedelta(hours=x) for x in li]

def test_calc_dist_1a(slot_hours):
    r = calc_distribution(10, smdl1)
    r = [x[0] for x in r]
    #print("r")
    #print(r)
    #print("ints_to_dts")
    #print(ints_to_dts([0, 1, 2, 3, 4, 5, 6, 12, 13, 14]))
    assert r == sorted(ints_to_dts([0, 1, 2, 3, 4, 5, 6, 14, 13, 20]))

def test_calc_dist_1b(slot_hours):
    r = calc_distribution(3, smdl1)
    r = [x[0] for x in r]
    assert r == sorted(ints_to_dts([3, 2, 4]))

def test_calc_dist_1c(slot_hours):
    r = calc_distribution(3, smdl1, buy_anyway_threshold=90.0)
    r = [x[0] for x in r]
    assert r == sorted(ints_to_dts([0, 1, 2, 3, 4, 5, 6, 23]))

def test_calc_dist_2(slot_hours):
    r = calc_distribution(1, smdl2)
    r = [x[0] for x in r]
    assert r == sorted(ints_to_dts([3]))

def test_calc_dist_3(slot_hours):
    r = calc_distribution(5, smdl2)
    r = [x[0] for x in r]
    assert r == sorted(ints_to_dts([3, 14, 4, 2, 13]))

def test_calc_dist_4(slot_hours):
    r = calc_distribution(6, smdl2)
    r = [x[0] for x in r]
    assert r == sorted(ints_to_dts([3, 4, 14, 13, 2, 5]))

def test_calc_dist_5(slot_hours):
    r = calc_distribution(9, smdl2)
    r = [x[0] for x in r]
    assert r == sorted(ints_to_dts([3, 4, 2, 14, 13, 6, 5, 1, 0]))

def test_calc_dist_6(slot_hours):
    r = calc_distribution(10, smdl2)
    r = [x[0] for x in r]
    assert r == sorted(ints_to_dts([3, 4, 2, 20, 6, 14, 5, 1, 0, 13]))
