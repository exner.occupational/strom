#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import pytest
from datetime import datetime, timedelta
from strom.service.power_schedule import PowerSchedule
from strom.util.action import Main

start_dt = datetime.fromisoformat('2023-03-12T16:00:00')
end_dt = datetime.fromisoformat('2023-04-12T16:00:00')

dt1 = datetime.fromisoformat('2023-03-12T16:00:00')
td1 = timedelta(hours=2)

dt2 = datetime.fromisoformat('2023-03-12T18:00:00')
td2 = timedelta(hours=1)

dt3 = datetime.fromisoformat('2023-03-12T18:30:00')
td3 = timedelta(hours=1)

dt4 = datetime.fromisoformat('2023-03-12T15:30:00')
td4 = timedelta(hours=4)

def test_action_1():
    ch = PowerSchedule(Main)
    assert ch.get_action(dt1) == Main.NONE

def test_add_intervals_1():
    ch = PowerSchedule(Main)
    ch.add_interval(dt1, td1, Main.H1, override=False)
    assert ch.get_action(dt1) == Main.H1
    assert ch.get_action(dt2) == Main.NONE

def test_add_intervals_2():
    ch = PowerSchedule(Main)
    ch.add_interval(dt2, td2, Main.H1, override=False)
    with pytest.raises(Exception):
        ch.add_interval(dt3, td3, Main.H1, override=False)
    ch.add_interval(dt3, td3, Main.OFF, override=True)
    assert ch.get_intervals(Main.NS, start_dt, end_dt) == []
    assert ch.get_intervals(Main.H1, start_dt, end_dt) == [(dt2, dt3 - dt2)]
    assert ch.get_intervals(Main.OFF, start_dt, end_dt) == [(dt3, td3)]

def test_add_intervals_3():
    ch = PowerSchedule(Main)
    ch.add_interval(dt4, td4, Main.H1, override=False)
    ch.add_interval(dt1, td1, Main.NS, override=True)
    assert ch.get_intervals(Main.H1, start_dt, end_dt) == [(dt1 + td1, dt4 + td4 - (dt1 + td1))]
    assert ch.get_intervals(Main.NS, start_dt, end_dt) == [(dt1, td1)]

def test_add_intervals_4():
    ch = PowerSchedule(Main)
    ch.add_interval(dt3, td3, Main.H1, override=False)
    ch.add_interval(dt1, td1, Main.H1, override=False)
    assert ch.get_intervals(Main.H1, start_dt, end_dt) == [(dt1, td1), (dt3, td3)]

def test_add_intervals_5():
    # test joining intervals
    ch = PowerSchedule(Main)
    ch.add_interval(dt2, td2, Main.H1, override=False)
    ch.add_interval(dt1, td1, Main.H1, override=False)
    assert ch.get_intervals(Main.H1, start_dt, end_dt) == [(dt1, td1 + td2)]

def test_del_intervals_before_1():
    ch = PowerSchedule(Main)
    ch.add_interval(dt1, td1, Main.H1, override=False)
    ch.add_interval(dt2, td2, Main.NS, override=False)
    ch.add_interval(dt3, td3, Main.OFF, override=True)
    assert len(ch.intervals) == 3
    ch.del_intervals_before(dt2)
    assert len(ch.intervals) == 3
    ch.del_intervals_before(dt3)
    assert len(ch.intervals) == 2

def test_get_intervals_1():
    """
    Assume all data without range will be dropped or truncated.
    """
    ch = PowerSchedule(Main)
    ch.add_interval(datetime(2023, 10, 10, 12, 0), timedelta(minutes=10), Main.H1, override=False)
        # gap
    ch.add_interval(datetime(2023, 10, 10, 12, 15), timedelta(minutes=15), Main.H1, override=False)
    ch.add_interval(datetime(2023, 10, 10, 12, 30), timedelta(minutes=5), Main.NS, override=False)
    ch.add_interval(datetime(2023, 10, 10, 12, 35), timedelta(minutes=5), Main.NS, override=False)
    ch.add_interval(datetime(2023, 10, 10, 12, 40), timedelta(minutes=15), Main.OFF, override=False)
    ch.add_interval(datetime(2023, 10, 10, 12, 55), timedelta(minutes=15), Main.H1, override=False)
    ch.add_interval(datetime(2023, 10, 10, 13, 10), timedelta(minutes=10), Main.H1, override=False)

    assert ch.get_intervals(Main.H1, datetime(2023, 10, 10, 11, 0), datetime(2023, 10, 10, 12, 56)) == \
        [
            (datetime(2023, 10, 10, 12, 0), timedelta(minutes=10)),
            (datetime(2023, 10, 10, 12, 15), timedelta(minutes=15)),
            (datetime(2023, 10, 10, 12, 55), timedelta(minutes=1))
        ]

    assert ch.get_intervals(Main.H1, datetime(2023, 10, 10, 12, 30), datetime(2023, 10, 10, 14, 00)) == \
        [
            (datetime(2023, 10, 10, 12, 55), timedelta(minutes=25))
        ]
