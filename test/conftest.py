#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import pytest
#from plyvel import destroy_db
from strom.service.db import DB

@pytest.fixture(scope="function")
def db(mocker):
    db_name = "/tmp/mocked.db"
    #try:
    #    destroy_db(db_name)
    #except:
    #    print("Could not destroy level DB")
    mocker.patch("strom.service.db.DB_FILE", db_name)
    db = DB()
    return db

@pytest.fixture(scope="function")
def slot_hours(mocker):
    slot_hours_1 = [23, 0, 1, 2, 3, 4, 5, 6, 7]
    #slot_hours_2 = [12, 13, 14, 20, 21]
    slot_hours_2 = [13, 14, 20] # Um 12 ist praktisch nie Strom verfügbar
    mocker.patch("strom.util.calc.slot_hours_1", slot_hours_1)
    mocker.patch("strom.util.calc.slot_hours_2", slot_hours_2)
