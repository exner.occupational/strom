#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

#from ..util import epoch
from strom.util.epoch import get_epoch_from_datetime, get_epoch_from_date, get_datetime_from_epoch, get_batches, datetime_to_short_rel_str, datetime_to_tzaware
from strom.util.const import VIENNA, UTC
from datetime import datetime, date
import pytest

def test_get_epoch_from_datetime_1():
    # Liefert Strompreisdaten vom 01.07.2019:
    # curl "https://api.awattar.at/v1/marketdata?start=1561939200000"

    dt = datetime(2019, 7, 1, 0, 0, 0, tzinfo=UTC)
    epoch = get_epoch_from_datetime(dt, ms=True)
    assert epoch == 1561939200000

def test_get_epoch_from_datetime_2():
    dt = datetime(2019, 7, 1, 0, 0, 0, tzinfo=VIENNA)
    epoch = get_epoch_from_datetime(dt, ms=True)
    assert epoch == 1561932000000

def test_get_epoch_from_datetime_3():
    dt = datetime(2019, 7, 1, 0, 0, 0)
    with pytest.raises(Exception):
        _ = get_epoch_from_datetime(dt, ms=True)

def test_get_epoch_from_date_1():
    d = date(2019, 7, 1)
    epoch = get_epoch_from_date(d, ms=True)
    assert epoch == 1561939200000

def test_get_epoch_from_date_2():
    d = date(2022, 12, 29)
    epoch = get_epoch_from_date(d, ms=True, vienna=True)
    assert epoch == 1672268400000

def test_get_datetime_from_epoch_1():
    dt = get_datetime_from_epoch(1561939200000, ms=True)
    assert dt == datetime(2019, 7, 1, 0, 0, 0, tzinfo=UTC)

def test_get_datetime_from_epoch_2():
    # dt = datetime(2019, 7, 1, 0, 0, 0, tzinfo=vienna)
    # dt.timestamp()
    # 1561935300.0
    dt = get_datetime_from_epoch(1561932000000, ms=True, vienna=True)
    assert dt == datetime(2019, 7, 1, 0, 0, 0, tzinfo=VIENNA)

def test_get_batches_1():
    l = get_batches(3, date(2022, 1, 1), date(2022, 1, 7))
    assert l == [
            (date(2022, 1, 1), date(2022, 1, 4)),
            (date(2022, 1, 4), date(2022, 1, 7)),
            ]

def test_get_batches_2():
    l = get_batches(3, date(2022, 1, 2), date(2022, 1, 7))
    assert l == [
            (date(2022, 1, 2), date(2022, 1, 5)),
            (date(2022, 1, 5), date(2022, 1, 7)),
            ]

def test_datetime_to_short_rel_str_1():
    # Winterzeit
    dt = datetime(2024, 2, 1, 9, 5, 7, 12345, tzinfo=VIENNA)
    # expect UTC output
    assert datetime_to_short_rel_str(dt=dt, today=date(2024, 2, 1)) == "T 0 08:05"
    assert datetime_to_short_rel_str(dt=dt, today=date(2024, 1, 31)) == "T+1 08:05"
    assert datetime_to_short_rel_str(dt=dt, today=date(2024, 2, 2)) == "T-1 08:05"
    # expect VIENNA output
    assert datetime_to_short_rel_str(dt=dt, vienna=True, today=date(2024, 2, 1)) == "T 0 09:05"
    assert datetime_to_short_rel_str(dt=dt, vienna=True, today=date(2024, 1, 31)) == "T+1 09:05"
    assert datetime_to_short_rel_str(dt=dt, vienna=True, today=date(2024, 2, 2)) == "T-1 09:05"

def test_datetime_to_short_rel_str_2():
    # Sommerzeit
    dt = datetime(2024, 8, 1, 9, 5, 7, 12345, tzinfo=VIENNA)
    # expect UTC output
    assert datetime_to_short_rel_str(dt=dt, today=date(2024, 8, 1)) == "T 0 07:05"
    assert datetime_to_short_rel_str(dt=dt, today=date(2024, 7, 31)) == "T+1 07:05"
    assert datetime_to_short_rel_str(dt=dt, today=date(2024, 8, 2)) == "T-1 07:05"
    # expect VIENNA output
    assert datetime_to_short_rel_str(dt=dt, vienna=True, today=date(2024, 8, 1)) == "T 0 09:05"
    assert datetime_to_short_rel_str(dt=dt, vienna=True, today=date(2024, 7, 31)) == "T+1 09:05"
    assert datetime_to_short_rel_str(dt=dt, vienna=True, today=date(2024, 8, 2)) == "T-1 09:05"

def test_datetime_to_short_rel_str_3():
    # Winterzeit
    dt = datetime(2024, 2, 1, 9, 5, 7, 12345, tzinfo=UTC)
    # expect UTC output
    assert datetime_to_short_rel_str(dt=dt, today=date(2024, 2, 1)) == "T 0 09:05"
    assert datetime_to_short_rel_str(dt=dt, today=date(2024, 1, 31)) == "T+1 09:05"
    assert datetime_to_short_rel_str(dt=dt, today=date(2024, 2, 2)) == "T-1 09:05"
    # expect VIENNA output
    assert datetime_to_short_rel_str(dt=dt, vienna=True, today=date(2024, 2, 1)) == "T 0 10:05"
    assert datetime_to_short_rel_str(dt=dt, vienna=True, today=date(2024, 1, 31)) == "T+1 10:05"
    assert datetime_to_short_rel_str(dt=dt, vienna=True, today=date(2024, 2, 2)) == "T-1 10:05"

def test_datetime_to_short_rel_str_3():
    # Sommerzeit
    dt = datetime(2024, 8, 1, 9, 5, 7, 12345, tzinfo=UTC)
    # expect UTC output
    assert datetime_to_short_rel_str(dt=dt, today=date(2024, 8, 1)) == "T 0 09:05"
    assert datetime_to_short_rel_str(dt=dt, today=date(2024, 7, 31)) == "T+1 09:05"
    assert datetime_to_short_rel_str(dt=dt, today=date(2024, 8, 2)) == "T-1 09:05"
    # expect VIENNA output
    assert datetime_to_short_rel_str(dt=dt, vienna=True, today=date(2024, 8, 1)) == "T 0 11:05"
    assert datetime_to_short_rel_str(dt=dt, vienna=True, today=date(2024, 7, 31)) == "T+1 11:05"
    assert datetime_to_short_rel_str(dt=dt, vienna=True, today=date(2024, 8, 2)) == "T-1 11:05"
    # Sommerzeit

def test_datetime_to_tzaware_1():
    assert datetime_to_tzaware('2024-03-24T06:00:00') == datetime.fromisoformat('2024-03-24T06:00:00+00:00')
    assert datetime_to_tzaware('2024-03-24T06:00:00', vienna_in=True) == datetime.fromisoformat('2024-03-24T05:00:00+00:00')
    assert datetime_to_tzaware('2024-03-24T06:00:00') == datetime.fromisoformat('2024-03-24T07:00:00+01:00')
    assert datetime_to_tzaware('2024-03-24T06:00:00', vienna_out=True) == datetime.fromisoformat('2024-03-24T07:00:00+01:00')
    assert datetime_to_tzaware('2024-03-24T06:00:00', vienna_out=True).isoformat() == '2024-03-24T07:00:00+01:00'
    assert datetime_to_tzaware('2024-05-24T06:00:00', vienna_out=True).isoformat() == '2024-05-24T08:00:00+02:00'
