#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import plyvel

def test_2():
    # Open or create a LevelDB database
    db = plyvel.DB('/tmp/test.db', create_if_missing=True)

    # Put data into the database
    db.put(b'key1', b'value1')
    db.put(b'key2', b'value2')

    # Get data from the database
    value = db.get(b'key1')
    print(value.decode('utf-8'))  # Decode bytes to string

    # Iterate through all key-value pairs
    for key, value in db:
        print(key.decode('utf-8'), value.decode('utf-8'))

    # Close the database when done
    db.close()
