# Steuerungs-Planung

Es können für folgende Elemente Profile erstellt werden:
* Rollo
* Ventilator
* Heizungs

Das Profil bestimmt den erzeugten Zeitplan.
Jedes Profil ist einem Tag zugeordnet, ab dem es angewendet wird.

# Profile

## Rollo
Beliebig viele Einträge der Art
- Betroffene Rollläden (Liste)
  - Schlafzimmer Türe
  - Wohnzimmer Fenster
  - Wohnzimmer Türe
- Betroffene Tage (Liste)
  - Montag - Sonntag
  - pro Tag Auswahl
    - nie
    - immer
    - nicht wenn Feiertag
    - nur wenn auch Feiertag
- Uhrzeit
  - absolut
  - relativ zu Sonnenaufgang
  - relativ zu Sonnenuntergang
- Aktion
  - Auf
  - Zu
  - Schutter
  - Prozentwert
- Kommentar

## Ventilator
- Betroffene Tage (Liste)
  - Montag - Sonntag
  - pro Tag Auswahl
    - nie
    - immer
    - nicht wenn Feiertag
    - nur wenn auch Feiertag
- Uhrzeit
  - absolut
  - relativ zu Sonnenaufgang
  - relativ zu Sonnenuntergang
- Aktion
  - Ein
  - Aus
- Kommentar

## Heizung

