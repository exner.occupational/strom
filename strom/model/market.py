#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from typing import List, Dict, Tuple, Union
from typing_extensions import Annotated
from pydantic import BaseModel, RootModel, Field, computed_field, field_validator, ValidationError
from pydantic.functional_validators import AfterValidator
from datetime import date, datetime, timedelta
from loguru import logger
from typeguard import typechecked
from strom.util.epoch import get_datetime_from_epoch, get_epoch_from_date, vienna_now, assert_epoch
from strom.util.const import UNIT_MINUTES


class MarketDataModel(BaseModel):
    """
    One hour of awattar raw marketdata.
    """
    start_timestamp: int # includes milis
    end_timestamp: int # includes milis
    marketprice: float
    unit: str

    @field_validator('start_timestamp', 'end_timestamp')
    @classmethod
    def epoch_must_include_milis(cls, v: int) -> int:
        assert_epoch(v, True)
        return v

    def start_epoch(self, ms: bool) -> int:
        """
        return with or without milis
        """
        return self.start_timestamp if ms else self.start_timestamp // 1000

    def end_epoch(self, ms: bool) -> int:
        """
        return with or without milis
        """
        return self.end_timestamp if ms else self.end_timestamp // 1000

    @typechecked
    def start_dt(self) -> datetime:
        return get_datetime_from_epoch(self.start_timestamp, ms=True, vienna=True)

    @typechecked
    def end_dt(self) -> datetime:
        return get_datetime_from_epoch(self.end_timestamp, ms=True, vienna=True)

class MarketDataList(RootModel):
    """
    Interval of awattar raw marketdata (hours).
    E. g. 14:00 of current day to 24:00 of next day.
    """
    root: List[MarketDataModel]
    @field_validator('root')
    @classmethod
    def sort_list(cls, v: List[MarketDataModel]) -> List[MarketDataModel]:
        v.sort(key=lambda obj: obj.start_timestamp)
        #v.sort(key=lambda obj: obj.marketprice)
        return v
    def __iter__(self):
        return iter(self.root)
    def __getitem__(self, item):
        return self.root[item]
    def __len__(self):
        return len(self.root)

#def check_hour_unit_fraction(v: int) -> int:
#    assert 60 % v == 0, f'{v} minutes are not a unit fraction of one hour'
#    return v

#HourUnitFractionMinutes = Annotated[int, Field(validate_default=True), AfterValidator(check_hour_unit_fraction)]

class SimpleMarketDataModel(BaseModel):
    """
    One unit of marketdata (e. g. 15 minutes)
    """
    start_dt: datetime
    #duration_minutes: HourUnitFractionMinutes = 60 # ToDo: Change to 15 minutes, refactoring needed
    marketprice: float
    @classmethod
    @typechecked
    def from_market_data_model(cls, market_data: MarketDataModel) -> List['SimpleMarketDataModel']:
        """
        Build a List of all SimpleMarketDataModel units within the hour specified by the MarketDataModel.
        E. g. if duration_minutes is set to 15 than 4 units are returned.
        """
        result = []
        for unit_count in range(60 // UNIT_MINUTES):
            smdm = cls(start_dt=market_data.start_dt() + timedelta(minutes=unit_count * UNIT_MINUTES), marketprice=market_data.marketprice)
            result.append(smdm)
        return result

class SimpleMarketDataList(RootModel):
    """
    Interval of marketdata units (e. g. 15 minute intervals).
    """
    root: List[SimpleMarketDataModel]
    @field_validator('root')
    @classmethod
    def sort_list(cls, v: List[SimpleMarketDataModel]) -> List[SimpleMarketDataModel]:
        v.sort(key=lambda obj: obj.start_dt)
        return v
    @classmethod
    @typechecked
    def from_market_data_list(cls, market_data_list: MarketDataList) -> 'SimpleMarketDataList':
        result = []
        for market_data in market_data_list:
            result.extend(SimpleMarketDataModel.from_market_data_model(market_data))
        return cls.model_validate(result)
    def __iter__(self):
        return iter(self.root)
    def __getitem__(self, item):
        return self.root[item]
    @typechecked
    def pstr(self) -> str:
        """
        Return pretty string representation
        """
        try:
            first_dt = self.root[0].start_dt
            deltas = [f"{int((simple_market_data.start_dt - first_dt).total_seconds() / 3600)}: {simple_market_data.marketprice}" for simple_market_data in self.root]
            return f'{first_dt}: {", ".join(deltas)}'
        except Exception:
            logger.exception("Error on pretty formatting of SimpleMarketDataList")
            fallback = str(self.root)
            logger.info(f"fallback: {fallback}")
            return fallback
    @typechecked
    def start_dt(self) -> datetime:
        return self[0].start_dt
    @typechecked
    def end_dt(self) -> datetime:
        return self[-1].start_dt + timedelta(minutes=UNIT_MINUTES)
