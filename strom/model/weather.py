#
# Copyright (c) 2024 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from datetime import date
from pydantic import BaseModel

class WeatherDayModel(BaseModel):
    """
    Aggregated weather data per day.

    Former data structure example:
    data of today (2024-03-23):
    {'temperature': 11.25, 'radiation': 140.91666666666666, 'windspeed': 14.2875, 'daylight_hours': 12.366666666666667}
    """
    day: date
    temperature: float
    radiation: float
    windspeed: float
    daylight_hours: float
