from enum import Enum, auto
from typing import List
from pydantic import BaseModel, RootModel, Field, field_validator

class HolidayMode(Enum):
    NEVER = auto()
    ALWAYS = auto()
    SKIP_HOLIDAYS = auto()
    ONLY_HOLIDAYS = auto()

class DaysModel(BaseModel):
    monday: HolidayMode = Field(HolidayMode.NEVER)
    tuesday: HolidayMode = Field(HolidayMode.NEVER)
    wednesday: HolidayMode = Field(HolidayMode.NEVER)
    thursday: HolidayMode = Field(HolidayMode.NEVER)
    friday: HolidayMode = Field(HolidayMode.NEVER)
    saturday: HolidayMode = Field(HolidayMode.NEVER)
    sunday: HolidayMode = Field(HolidayMode.NEVER)


class TimeMode(Enum):
    ABSOLUTE = auto()
    REL_SUNRISE = auto()
    REL_SUNSET = auto()

class TimeModel(BaseModel):
    hours: int
    minutes: int
    mode: TimeMode = Field(TimeMode.ABSOLUTE)


#class DaysList(RootModel):
#    """
#    Interval of awattar raw marketdata (hours).
#    E. g. 14:00 of current day to 24:00 of next day.
#    """
#    root: List[DaysModel]
#    @field_validator('root')
#    @classmethod
#    def sort_list(cls, v: List[MarketDataModel]) -> List[MarketDataModel]:
#        v.sort(key=lambda obj: obj.start_timestamp)
#        #v.sort(key=lambda obj: obj.marketprice)
#        return v
#    def __iter__(self):
#        return iter(self.root)
#    def __getitem__(self, item):
#        return self.root[item]
#    def __len__(self):
#        return len(self.root)

