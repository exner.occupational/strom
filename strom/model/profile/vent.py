from pydantic import BaseModel, RootModel, Field, field_validator
from typing import List
from strom.model.profile.common import HolidayMode, DaysModel, TimeMode, TimeModel

class VentProfile(BaseModel):
    """
    Vent Action Definition
    """
    days: DaysModel = Field(DaysModel)
    time: TimeModel = Field(TimeModel)

class VentProfileList(RootModel):
    """
    List of Vent Action Definitions
    """
    root: List[VentProfile]
    #@field_validator('root')
    #@classmethod
    #def sort_list(cls, v: List[VentProfile]) -> List[VentProfile]:
    #    v.sort(key=lambda obj: obj.start_timestamp)
    #    #v.sort(key=lambda obj: obj.marketprice)
    #    return v
    def __iter__(self):
        return iter(self.root)
    def __getitem__(self, item):
        return self.root[item]
    def __len__(self):
        return len(self.root)

