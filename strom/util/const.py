#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import os
from dateutil.relativedelta import relativedelta
from zoneinfo import ZoneInfo
from pathlib import Path

LATITUDE = 48.1742
LONGITUDE = 16.2559
WEATHER_HISTORY = "weather_history"
WEATHER_FORECAST = "weather_forecast"
MARKET_DATA = "market_data"
VIENNA = ZoneInfo('Europe/Vienna')
UTC = ZoneInfo('UTC')
BUY_ANYWAY_THRESHOLD_MAIN = 55.0 # EUR/MWh (=3ct/KWh)
BUY_ANYWAY_THRESHOLD_BOILER = 50.0 # EUR/MWh (=2ct/KWh)

#UNIT_HOUR_FRACTION = 4
UNIT_HOUR_FRACTION = 1 # ToDo: Change to 4 respectively 15 minutes, refactoring needed
UNIT_MINUTES = 60 // UNIT_HOUR_FRACTION


TEMPLATE_FOLDER = os.path.abspath('strom/service/templates')
#LOG_FILE = '/tmp/strom.log'
HOME = str(Path.home())
DB_FILE = f'{HOME}/git/heating_data/strom.db'
#template_folder = '/home/pi/git/strom/templates'
#print(template_folder)

# Energiepreise "Nachtstrom Speicherheizung" ab 01.09.2022:
# 26.6675 inkl. 20% USt. und 6% Gebrauchsabgabe
# awattar bietet über das API Netto-Preise an.
# Daher 20% und 6% korrigieren sowie ca. 2 Cent weniger Nebenkosten abziehen.
# 26.6675 / 1.06 / 1.2 - 2 = 18.9650 Cent/kWh
H1_PRICE = 144 # EUR/MWh lt. gogreenenergy Preisblatt vom 01.08.2004.
H1_FIXED = True # True: 24x7 H1_PRICE, False: Auch Stunden-Tarif
NS_ADD_PRICE = 49 # Mai 2024: (0.0288 + 0.00866 + 0.001) * 1.06 * 1.2 *100 = 4.9 ct/kWh
H1_ADD_PRICE = NS_ADD_PRICE + 28 # EUR/MWh geschätzte Netz-Mehrkosten bei Tagstrom im Vergleich zu Nachtstrom
"""
Zeitfenster beim Nachtstrom-Zähler
Mo - So
* 20:30 - 22:00
* 23:00 - 07:30
* variabel 2h zwischen 12:00 - 15:00
Zeitfenster beim Nachtstrom-Zähler
            14 15 16 17 18 19 20 21 22 23 00 01 02 03 04 05 06 07 08 09 10 11 12 13
            ??                 x XX    XX XX XX XX XX XX XX XX x              ?? ??

"""
NO_POWER_HOURS = [8, 9, 10, 11, 12, 15, 16, 17, 18, 19, 22] # Stunden ohne Nachtstrom (um 12:00 ist meist kein Nachtstrom)
ANY_POWER_HOURS = [7, 12, 20]
ANSCHLUSSLEISTUNG = 11.5 # Annahme: 11.5 kW = 50 A

PORT = 5000
