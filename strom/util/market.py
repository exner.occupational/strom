#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import requests
from typing import List, Dict, Tuple #, Union
from datetime import date, datetime, timedelta #, datetime
from loguru import logger
from typeguard import typechecked
from strom.service.db import DB, DBDomain
from strom.model.market import MarketDataList, SimpleMarketDataList #, MarketDataModel, SimpleMarketDataModel
from strom.util.misc import filter_dict
from strom.util.epoch import get_datetime_from_epoch, get_epoch_from_date, vienna_now
from strom.util.const import H1_PRICE, H1_FIXED, NS_ADD_PRICE, H1_ADD_PRICE, NO_POWER_HOURS


@typechecked
def get_start_end_epoch(my_date: date) -> (int, int):
    """
    Z. B.: 01.04.2023 -> epochs of (02.04.2023 00:00.000, 03.04.2023 00:00.000) -> (1680386400000, 1680472800000)
    Z. B.: 02.04.2023 -> epochs of (03.04.2023 00:00.000, 04.04.2023 00:00.000) -> (1680472800000, 1680559200000)
    """
    #d1 = timedelta(days=-1 if dt.hour < 15 else 0)
    #d2 = timedelta(days=2)
    #return (get_epoch_from_datetime(get_midnight(dt + d1), ms=True), get_epoch_from_datetime(get_midnight(dt + d1 + d2), ms=True))
    return tuple([get_epoch_from_date(d=my_date + timedelta(days=tdelta), ms=True, vienna=True) for tdelta in [1, 2]])


@typechecked
def request_market_data(epoch_start: int, epoch_end: int) -> Dict:
    """
    Fragt das Strompreis-API ab und gibt die Daten als 1:1 von awattar
    :return: json
    """
    logger.info(f"epoch_start: {epoch_start} / epoch_end: {epoch_end}")
    response = requests.get(f"https://api.awattar.at/v1/marketdata?start={epoch_start}&end={epoch_end}")
    sc = response.status_code
    if sc != 200:
        raise Exception("No response from aWATTar")
    return response.json()


@typechecked
class MarketHandler():
    """
    To hold raw data from awattar.
    """

    def __init__(self):
        self.md_dict: Dict[datetime, MarketDataList] = {} # hold marketdata of two days

    #def update(self, epoch: int):
    def update(self, vnow: datetime):
        """
        Get marketdata of two days. Which dates depends on whether it is before or after 2 p.m.
        """
        try:
            logger.info(f"update MarketData at time: {vnow}")
            if vnow.hour >= 14:
                tdelta = timedelta(days=0)
            else:
                tdelta = timedelta(days=-1)

            # Before 14:00 --> dates = [<the day before yesterday>, <yesterday>]
            # After 14:00  --> dates = [<yesterday>, <today>]
            dates = [vnow.date() + tdelta + timedelta(days=-1), vnow.date() + tdelta]

            md_dict = filter_dict(self.md_dict, dates) # delete all keys not in dates
            for my_date in dates:
                date_count = len(md_dict.get(my_date, [])) # get number of hours or 0 if non existent
                if not 22 < date_count < 26: # plausi check, take account of DST (Daylight Saving Time) time adjustment
                    market_data = request_market_data(*get_start_end_epoch(my_date))
                    md_dict[my_date] = MarketDataList.model_validate(market_data["data"])
            self.md_dict = md_dict
        except Exception as e:
            logger.error(f"update of MarketData failed: {str(e)}")
            raise


    def dump(self, db: DB) -> None:
        for my_date in self.md_dict:
            for mdm in self.md_dict[my_date]:
                db.put(DBDomain.PRICE, mdm.start_epoch(False), mdm.marketprice)


    def get_simple_market_data_list(self, as_of: date | None = None, adjust: bool = True) -> SimpleMarketDataList:
        """
        Gibt awattar-Daten als SimpleMarketDataList zurück
        :return: SimpleMarketDataList
        """
        tmp_list = []
        if as_of:
            # Take market data of one day
            tmp_list.extend(self.md_dict[as_of].root)
        else:
            # Take market data of all (two) days
            for obj in self.md_dict.values():
                tmp_list.extend(obj.root)
        market_data_list = MarketDataList.model_validate(tmp_list)
        # sort market data by start time
        #market_data_list.sort(key=lambda obj: obj.start_timestamp)

        simple_market_data_list = SimpleMarketDataList.from_market_data_list(market_data_list)
        if adjust:
            simple_market_data_list = MarketHandler._adjust_simple_market_data_list(simple_market_data_list)

        logger.info(f"return market_data_list: {simple_market_data_list.pstr()}")
        return simple_market_data_list

    def _adjust_simple_market_data_list(simple_market_data_list: SimpleMarketDataList) -> SimpleMarketDataList:
        """
        Den Preis bei allen Stunden die nicht bezogen werden können auf den Tagstrompreis setzen.
        (Weil der Nachtstromzähler abgeschalten ist)
        :full_market_data: [[<hour>, <price>], [<hour>, <price>], ...]
        :return: [[<hour>, <price>], [<hour>, <price>], ...]
        """

        """
        Zeitfenster beim Nachtstrom-Zähler
        Mo - So
        * 20:30 - 22:00
        * 23:00 - 07:30
        * variabel 2h zwischen 12:00 - 15:00
        """

        for item in simple_market_data_list:
            if item.start_dt.hour in NO_POWER_HOURS:
                if H1_FIXED:
                    item.marketprice = H1_PRICE + H1_ADD_PRICE # EUR/MWh # Fixpreis
                else:
                    item.marketprice = item.marketprice + H1_ADD_PRICE # Netzkosten in EUR/MWh bei Tagstrom addieren
            else:
                item.marketprice = item.marketprice + NS_ADD_PRICE # Netzkosten in EUR/MWh bei Nachtstrom addieren
        return simple_market_data_list

    def get_chart_market_data(self) -> List[Tuple[datetime, float]]:
        """
        Diese Funktion gibt alle awattar-Daten als 2D-Array zurück
        :return: [[<hour datetime>, <price>], [<hour datetime>, <price>], ...]
        """
        market_data_list = []
        # Iterate through the dictionary values and concatenate them
        for obj in self.md_dict.values():
            market_data_list.extend(obj.root)
        market_data_list.sort(key=lambda obj: obj.start_timestamp)

        all = []
        for obj in market_data_list:
            all.append((get_datetime_from_epoch(obj.start_timestamp, ms=True, vienna=True), obj.marketprice))
            all.append((get_datetime_from_epoch(obj.end_timestamp, ms=True, vienna=True), obj.marketprice))
        return all

if __name__ == "__main__":
    mh = MarketHandler()
    mh.update(vienna_now())
    logger.info(f"simple_market_data_list: {mh.get_simple_market_data_list().pstr()}")
