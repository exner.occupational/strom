#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import io
from loguru import logger
from datetime import datetime, timedelta
from typing import Dict, List, Tuple
from typeguard import typechecked
from strom.util.const import VIENNA
from strom.util.epoch import get_datetime_from_epoch
from strom.util.action import ColorizedEnum, Main, Avail, Boiler, Vent
from strom.util.weather import Weather
from strom.service.power_schedule import main_schedule #, boiler_schedule, vent_schedule
from strom.service.db import DB, DBDomain
from typeguard import typechecked

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
#from matplotlib.figure import Figure
import matplotlib.dates as mdates
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas


@typechecked
def get_vertical_line_times(start_time, end_time, interval_min = 60) -> List[datetime]:
    interval_td = timedelta(minutes=interval_min)
    start_min = (start_time + timedelta(minutes=interval_min - start_time.minute % interval_min)).replace(second=0, microsecond=0)
    #end_min = (end_time - timedelta(minutes=(end_time.minute % interval_min or interval_min))).replace(second=0, microsecond=0)
    end_min = (end_time - timedelta(minutes=end_time.minute % interval_min)).replace(second=0, microsecond=0)
    vertical_line_times = [start_min + i * interval_td for i in range(int((end_min - start_min) / interval_td))]
    return vertical_line_times

@typechecked
def draw_vertical_line_times(ax, vertical_line_times: List[datetime], beyond_top: bool = False) -> None:
    """
    Draws vertical lines every e. g. 60 minutes.

    :beyond_top: If True, the lines will connect to the upper chart.
    """
    ymin = 0
    ymax = 1.4 if beyond_top else 1
    for line_time in vertical_line_times:
        ax.axvline(line_time, ymin=ymin, ymax=ymax, color='gray', linestyle='--', linewidth=0.5, zorder=0, clip_on=False)

@typechecked
def get_bar_coords(count: int, rand: int = 1, inter: int = 1, breite: int = 5) -> Tuple[List[Tuple[int, int]], Tuple[int, int], List[float]]:
    """
    :rand: Abstand oben / unten
    :inter: Abstand zwischen den Linien
    :breite: Breite der Linien
    :count:  Anzahl der Linien

    :return: Tuple: list of tuple of y_min + yheight values, tuple of botom + top, list of ticks
    """
    y_ranges = [(rand + n * (breite + inter), breite) for n in range(count)] # broken_barh param: yrange
    bottom_top = (0, rand * 2 + breite * count + inter * (count - 1)) # set_ylim params: bottom, top
    ticks = [rand + breite / 2 + (breite + inter) * x for x in range(count)] # set_yticks param: ticks
    return (y_ranges, bottom_top, ticks)


# Stem plot
@typechecked
def create_prices(date_list: List[datetime], price_list: List[float], ns_price: float, vienna_now: datetime, db: DB):
    #logger.debug(date_list)
    #logger.debug(vienna_now)

    # get start and end time
    start_time = date_list[0]
    end_time = date_list[-1]
    vertical_line_times = get_vertical_line_times(start_time, end_time)

    # Setup Chart
    fig, axs = plt.subplots(nrows=2, sharex=True, height_ratios=[3, 1])
    fig.set_size_inches(16, 8)
    fig.set_dpi(75)

    #######
    # Marketdata Chart

    ax = axs[0]
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d. %b %H:%M', tz=VIENNA))
    #ax.xaxis.set_major_locator(mdates.AutoDateLocator())
    #ax.xaxis.set_major_locator(mdates.HourLocator(interval=4))  # Set interval to 4 hours
    ax.xaxis.set_major_locator(mdates.HourLocator(byhour=range(0, 24, 4), tz=VIENNA))  # Set interval to 4 hours (aligned)
    fig.autofmt_xdate()
    # horizontal line on Nachtstrom-Preis
    ax.axhline(y=ns_price, color='r', linestyle='-')
    # Draw vertical lines every quarter hour
    draw_vertical_line_times(ax, vertical_line_times)
    # vertical lines on midnight
    for dt in date_list:
        if dt.hour == 0:
            ax.axvline(x=dt, color='g', linestyle='dashed')
    # vertical lines on current time
    ax.axvline(x=vienna_now, color='b', linestyle='-')
    ax.plot(date_list, price_list)
    ax.set_ylabel('Preis (EUR/MWh)', color='b')
    ax.yaxis.tick_right()
    # diagram starts at zero unless negative prices
    ax.set_ylim(ymin=min(min(price_list), 0))
    # background colors on heating intervals
    for action in main_schedule.t:
        for i in main_schedule.get_intervals(action, start_time, end_time):
            ax.axvspan(i[0], i[0] + i[1], facecolor=action.color, alpha=0.5) # on intervals

    #
    #######

    #######
    # Relay State Chart

    ax = axs[1]
    # Draw vertical lines every quarter hour
    draw_vertical_line_times(ax, vertical_line_times, True)
    # Create a list of datetime values
    #x = [start_time + timedelta(hours=i) for i in [0, 8, 16, 24, 32]]
    # Create a list of values for each broken bar
    #values = [10, 40, 20, 40, 100]
    #bars = [(j, timedelta(minutes=values[i])) for i, j in enumerate(x)]

    # Plot the broken bars
    #print(bars)
    # Layout Params
    count = 4
    (y_ranges, bottom_top, ticks) = get_bar_coords(count=count)
    # two lines of demo data
    #for y_range in y_ranges[:-2]:
    #    ax.broken_barh(bars, y_range, facecolor='blue')

    ## Main data history
    #for action in Main:
    #    x_ranges = [(i[0], i[1]) for i in db.get_intervals_strict(DBDomain.MAIN, action, start_time, vienna_now)]
    #    ax.broken_barh(x_ranges, y_ranges[count - 1], facecolor=action.color, alpha=1)

    ## Avail data
    #for action in Avail:
    #    x_ranges = [(i[0], i[1]) for i in db.get_intervals_strict(DBDomain.AVAIL, action, start_time, vienna_now)]
    #    ax.broken_barh(x_ranges, y_ranges[count - 2], facecolor=action.color, alpha=1)

    # History
    action_enums = [Avail, Main, Boiler, Vent]
    action_db_mapping = {Avail: DBDomain.AVAIL, Main: DBDomain.MAIN, Boiler: DBDomain.BOILER, Vent: DBDomain.VENT}
    for num, action_enum in enumerate(action_enums):
        for action in action_enum:
            x_ranges = [(i[0], i[1]) for i in db.get_intervals_strict(action_db_mapping[action_enum], action, start_time, vienna_now)]
            ax.broken_barh(x_ranges, y_ranges[count - 1 - num], facecolor=action.color, alpha=1)

    # Planned
    #for num, schedule in enumerate(schedule_dict.values()):
    for num, schedule in enumerate(ColorizedEnum.get_power_schedules()):
        for action in schedule.t:
            x_ranges = [(i[0], i[1]) for i in schedule.get_intervals(action, vienna_now, end_time)]
            ax.broken_barh(x_ranges, y_ranges[count - 2 - num], facecolor=action.color, alpha=0.5)

    ax.set_ylim(*bottom_top)
    ax.set_yticks(ticks)
    ax.set_yticklabels(['Vent', 'Boiler', 'Main', 'Avail'])

    #
    #######

    # Finalize Picture
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    output_value = output.getvalue()
    plt.clf()
    plt.close(fig)
    return output_value

@typechecked
def create_weather(weather: Weather, now):
    hourly_fields = ["temperature_2m", "shortwave_radiation", "windspeed_10m"]
    label = ["Temperatur (°C)", "Sonne (W/m²)", "Wind (km/h)"]
    hourly_colors = ["purple", "orange", "blue"]

    #prep_hourly = {}
    ## span each time util next hour
    ## [1, 2, 3] -> [1, 2, 2, 3, 3, 4]
    #prep_hourly["time"] = [datetime.fromisoformat(h) + timedelta(hours = i) for h in hourly["time"] for i in range(2)]
    ## each weather data point twice
    ## [1, 2, 3] -> [1, 1, 2, 2, 3, 3]
    #for f in hourly_fields:
    #    prep_hourly[f] = [h for h in hourly[f] for i in range(2)]
    ##prep_date_list = [h for h in prep_hourly["time"]]

    prep_hourly = weather.get_graph_weather_data()

    prep_date_list = prep_hourly["time"]
    #logger.debug(prep_date_list)
    #logger.debug(now)

    # get start and end time
    start_time = prep_date_list[0]
    end_time = prep_date_list[-1]
    vertical_line_times = get_vertical_line_times(start_time, end_time)

    fig, axs = plt.subplots(nrows=len(hourly_fields), sharex=True)
    fig.set_size_inches(16, 6)
    fig.set_dpi(75)
    axs[0].xaxis.set_major_formatter(mdates.DateFormatter('%d. %b %H:%M', tz=VIENNA))
    #axs[0].xaxis.set_major_locator(mdates.AutoDateLocator())
    axs[0].xaxis.set_major_locator(mdates.HourLocator(byhour=range(0, 24, 4), tz=VIENNA))  # Set interval to 4 hours (aligned)
    fig.autofmt_xdate()
    for ax in axs:
        # Draw vertical lines every quarter hour
        draw_vertical_line_times(ax, vertical_line_times)
        # vertical lines on midnight
        for dt in prep_date_list:
            if dt.hour == 0:
                ax.axvline(x=dt, color='g', linestyle='dashed')
    # vertical lines on current time
        ax.axvline(x=now, color='b', linestyle='-')
    # plot all weather data
    for i, f in enumerate(hourly_fields):
        axs[i].plot(prep_date_list, prep_hourly[f], hourly_colors[i]) # plotting
        axs[i].set_ylabel(label[i], color=hourly_colors[i])
        axs[i].yaxis.tick_right()
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    output_value = output.getvalue()
    plt.clf()
    #plt.close("all")
    plt.close(fig)
    return output_value

@typechecked
def create_avail_state(data_avail: Dict[int, Avail], data_state: Dict[int, Main]):
    fig, ax = plt.subplots(1)
    fig.set_size_inches(8, 4)
    fig.set_dpi(75)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d. %b %H:%M', tz=VIENNA))
    #plt.gca().xaxis.set_major_locator(mdates.HourLocator())
    ax.xaxis.set_major_locator(mdates.AutoDateLocator())
    fig.autofmt_xdate()
    ax.fill_between([get_datetime_from_epoch(key, False, True) for key in data_avail.keys()], [int(value.value) for value in data_avail.values()], color='lightblue', alpha=0.5, label='avail')
    ax.plot([get_datetime_from_epoch(key, False, True) for key in data_state.keys()], [value.value for value in data_state.values()], marker='o', color='red', linestyle='None', label='state')
    plt.yticks(range(len(Main)), list(Main))  # Set y-axis ticks as enum values
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    output_value = output.getvalue()
    plt.clf()
    #plt.close("all")
    plt.close(fig)
    return output_value

@typechecked
def create_avail_state_2(data_avail: Dict[int, Avail], data_state: Dict[int, Main]):
    fig, axs = plt.subplots(nrows=2, sharex=True)
    fig.set_size_inches(8, 6)
    fig.set_dpi(75)
    axs[0].xaxis.set_major_formatter(mdates.DateFormatter('%d. %b %H:%M', tz=VIENNA))
    axs[0].xaxis.set_major_locator(mdates.AutoDateLocator())
    fig.autofmt_xdate()
    #for ax in axs:
    ## vertical lines on midight
    #    for dt in prep_date_list:
    #        if dt.hour == 0 and dt.minute == 0 and dt.second == 0:
    #            ax.axvline(x=dt, color='g', linestyle='dashed')
    #for i, f in enumerate(hourly_fields):
        #ax = ax.twinx()
    #axs[0].plot(prep_date_list, prep_hourly[f], hourly_colors[i]) # plotting
    #axs[0].fill_between(data_avail.keys(), [int(value) for value in data_avail.values()], color='lightblue', alpha=0.5, label='avail')
    axs[0].fill_between([get_datetime_from_epoch(key, False, True) for key in data_avail.keys()], [value.value for value in data_avail.values()], color='lightblue', alpha=0.5)
    axs[0].set_ylabel("avail", color='b')
    axs[0].yaxis.tick_right()
    axs[1].plot([get_datetime_from_epoch(key, False, True) for key in data_state.keys()], [value.value for value in data_state.values()], marker='o', color='red', linestyle='-')
    axs[1].set_ylabel("state", color='r')
    axs[1].set_yticks(range(1, len(Main) + 1), list(Main))  # Set y-axis ticks as enum values
    axs[1].yaxis.tick_right()
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    output_value = output.getvalue()
    plt.clf()
    #plt.close("all")
    plt.close(fig)
    return output_value


def create_avail(date_list, avail_list, title="avail"):
    # Create a Matplotlib figure
    fig, ax = plt.subplots(1)
    fig.set_size_inches(8, 4)
    fig.set_dpi(75)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d. %b %H:%M', tz=VIENNA))
    ax.xaxis.set_major_locator(mdates.AutoDateLocator())
    fig.autofmt_xdate()
    # plot the data
    #plt.plot(date_list, avail_list)
    plt.fill_between(date_list, avail_list, color='lightblue', alpha=0.5, label='avail')
    ax.set_ylabel('Nachstrom verfügbar', color='b')
    ax.set_ylim(ymin=0, ymax=1)
    ax.yaxis.tick_right()
    # diagram starts at zero unless negative prices
    #ax.set_ylim(ymin=min(min(price_list), 0))
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    output_value = output.getvalue()
    plt.clf()
    #plt.close("all")
    plt.close(fig)
    return output_value
