#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from typeguard import typechecked

@typechecked
def filter_dict(input_dict: dict, key_list: list) -> dict:
    """
    Create a new dictionary with only the keys that exist in the key_list
    """
    filtered_dict = {key: input_dict[key] for key in key_list if key in input_dict}
    return filtered_dict
