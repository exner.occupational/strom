#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from typeguard import typechecked
from time import sleep
from enum import Enum
from typing import Tuple

# https://stackoverflow.com/questions/12680080/python-enums-with-attributes
class ColorizedEnum(Enum):

    @classmethod
    def get_power_schedule(cls):
        return cls._schedule

    @classmethod
    def get_power_schedules(cls):
        schedules = []
        for t in cls._registry.values():
            if t._schedule:
                schedules.append(t._schedule)
        return schedules

    def __new__(cls, *args, **kwds):
        obj = object.__new__(cls)
        obj._value_ = args[0]
        return obj

    # ignore the first param since it's already set by __new__
    def __init__(self, _: str, color):
        self._color_ = color

    # this makes sure that the color is read-only
    @property
    def color(self):
        return self._color_

    @classmethod
    def from_str(cls, param):
        return ColorizedEnum._registry[param]

class Main(ColorizedEnum):
    NONE = 0, "red"
    OFF = 1, "grey"
    NS = 2, "blue"
    H1 = 3, "green"
    ANY = 5, "purple" # olive? https://matplotlib.org/stable/gallery/color/named_colors.html
    FAULTY = 9, "orange"
# Main._schedule is set by PowerSchedule
Main._schedule = None

class Boiler(ColorizedEnum):
    NONE = 0, "red"
    OFF = 1, "grey"
    ON = 2, "cyan"
# Boiler._schedule is set by PowerSchedule
Boiler._schedule = None

class Vent(ColorizedEnum):
    NONE = 0, "red"
    OFF = 1, "grey"
    ON = 2, "cyan"
# Vent._schedule is set by PowerSchedule
Vent._schedule = None

class Avail(ColorizedEnum):
    NONE = 0, "red"
    OFF = 1, "grey"
    ON = 2, "cyan"
Avail._schedule = None

ColorizedEnum._registry = {Main.__name__: Main, Boiler.__name__: Boiler, Vent.__name__: Vent, Avail.__name__: Avail}
