# Code Code based on ChatGPT and Gemini

from math import radians, cos, sin, tan, acos, atan2, degrees
from datetime import datetime, timedelta
from strom.util.const import LATITUDE, LONGITUDE, UTC #, VIENNA

# Code based on ChatGPT
def calc_sunrise_sunset_a(date, latitude = LATITUDE, longitude = LONGITUDE):
    # Convert latitude and longitude to radians
    lat_rad = radians(latitude)
    lon_rad = radians(longitude)

    # Calculate the day of the year
    day_of_year = date.timetuple().tm_yday

    # Calculate the solar declination angle (delta)
    solar_declination = 23.45 * sin(radians(360 * (284 + day_of_year) / 365))

    # Calculate the hour angle at sunrise and sunset (H0)
    hour_angle = acos(-tan(lat_rad) * tan(radians(solar_declination)))

    # Calculate the solar noon
    solar_noon = 12 + (longitude / 15) - (4 * (180 - date.timetuple().tm_yday) / 360)

    # Calculate the sunrise and sunset times
    sunrise = solar_noon - (degrees(hour_angle) / 15)
    sunset = solar_noon + (degrees(hour_angle) / 15)

    # Convert sunrise and sunset to datetime objects
    sunrise = datetime(date.year, date.month, date.day, int(sunrise), int((sunrise - int(sunrise)) * 60), tzinfo=UTC)
    sunset = datetime(date.year, date.month, date.day, int(sunset), int((sunset - int(sunset)) * 60), tzinfo=UTC)

    return sunrise, sunset



# Code based on Gemini
def calc_sunrise_sunset_b(date, latitude = LATITUDE, longitude = LONGITUDE):
    """
    Calculates approximate sunrise and sunset times for a given date and location.

    Args:
        date (datetime.date): Date for which to calculate sunrise and sunset.
        latitude (float): Latitude of the location in degrees (north positive).
        longitude (float): Longitude of the location in degrees (east positive).

    Returns:
        tuple: Tuple containing two UTC datetime objects (sunrise offset, sunset offset).
              None values indicate calculation failure due to invalid input.

    Notes:
        This is a simplified approximation and may not be accurate for all locations and dates.
    """

    # Convert date to day of the year (1-365)
    doy = date.timetuple().tm_yday

    # Constants (adjust for better accuracy in your specific region)
    J_DEC = 356.3  # Julian day constant
    J_0 = 285  # Approximate Julian day at the winter solstice

    # Calculate solar declination angle (radians)
    declination_rad = radians(0.0034 * (doy - J_0))

    # Calculate hour angle at sunrise/sunset (radians)
    cos_h = (sin(radians(latitude)) * sin(declination_rad)) / -cos(declination_rad)

    # Check for invalid conditions (polar day/night)
    if abs(cos_h) > 1:
        return None, None

    # Calculate sunrise/sunset hour angles (radians)
    h_sunrise = acos(cos_h)
    h_sunset = -h_sunrise

    # Calculate time offsets from solar noon (hours)
    time_offset = h_sunrise / radians(15)  # 15 degrees per hour

    # Adjust for UTC offset and return timedelta objects
    sunrise_offset = timedelta(hours=time_offset - (longitude / 15))
    sunset_offset = timedelta(hours=time_offset + (longitude / 15))

    # Calculate UTC datetime objects
    sunrise = datetime(date.year, date.month, date.day, tzinfo=UTC) + sunrise_offset
    sunset = datetime(date.year, date.month, date.day, tzinfo=UTC) + sunset_offset

    return sunrise, sunset
