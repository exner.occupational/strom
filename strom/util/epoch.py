#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from .const import VIENNA, UTC
from datetime import datetime, date, timedelta
from time import time
from typeguard import typechecked


# defining the timezone
#tz = pytz.timezone('Europe/Vienna')

@typechecked
def vienna_now() -> datetime:
    """
    Return current datetime (vienna time zone)
    """
    return datetime.now(VIENNA)

@typechecked
def epoch_now(ms: bool) -> int:
    """
    Return current epoch
    """
    return int(time()) * (1000 if ms else 1)

@typechecked
def epoch_add_timedelta(epoch: int, ms: bool, td: timedelta) -> int:
    """
    Add time delta to epoch
    """
    # check and convert mili seconds
    assert_epoch(epoch, ms)
    epoch = epoch / 1000 if ms else epoch
    # Convert the input epoch time to a datetime object
    input_datetime = datetime.fromtimestamp(epoch)
    # Subtract the specified timedelta from the input datetime
    result_datetime = input_datetime + td
    # Convert the result datetime back to an epoch timestamp
    return int(result_datetime.timestamp())  * (1000 if ms else 1)

def get_midnight(dt: datetime):
    return dt.replace(hour=0, minute=0, second=0, microsecond=0)

def get_epoch_from_datetime(dt: datetime, ms: bool):
    if dt.tzinfo is None:
        raise Exception("Only timezone aware allowed")
    return int(dt.timestamp()) * (1000 if ms else 1)

def get_epoch_from_date(d: date, ms: bool, vienna: bool = False):
    tz = VIENNA if vienna else UTC
    dt = datetime(d.year, d.month, d.day, 0, 0, 0, tzinfo=tz)
    return int(dt.timestamp()) * (1000 if ms else 1)

def get_datetime_from_epoch(epoch: int, ms: bool, vienna: bool = False):
    tz = VIENNA if vienna else UTC
    epoch = epoch / 1000 if ms else epoch
    return datetime.fromtimestamp(epoch, tz)
    #dt = datetime.utcfromtimestamp(int(epoch / 1000))
    #dt = dt.astimezone(pytz.timezone('Europe/Vienna'))
    #return dt

#def get_date_from_epoch(epoch: int, ms: bool, vienna: bool = False):
#    dt = get_datetime_from_epoch(epoch, ms, vienna)
#    return dt.date()

def get_batches(n: int, date_from, date_to: date):
    """
    return n-sized batches of timeframes between date_fron amd date_to (last batch may be smaller)
        batches: list of (<date from>, <date to>)
    """
    assert isinstance(date_from, date)
    assert isinstance(date_to, date)
    batches = []
    while (date_next := date_from + timedelta(days=n)) <= date_to:
        batches.append((date_from, date_next))
        date_from = date_next
    if date_from != date_to:
        batches.append((date_from, date_to))
    return batches

@typechecked
def assert_epoch(epoch: int, ms: bool):
    """
    Plausibility check whether epoch is valid.
    """
    assert 1000000000 * (1000 if ms else 1) < epoch < 9999999999 * (1000 if ms else 1)

@typechecked
def datetime_to_tzaware(dt: str | datetime, vienna_in: bool = False, vienna_out: bool = False):
    """
    Convert a datetime to a timezone aware datetime object with defined timezone.

    dt: string in datetime ISO format or datetime object. Assume UTC if no timezone specified.
    vienna_in: timezone of tz
    vienna: True is vienna timezone, False is UTC
    """
    tz_in = VIENNA if vienna_in else UTC
    tz_out = VIENNA if vienna_out else UTC
    if isinstance(dt, str):
        dt = datetime.fromisoformat(dt)
    if dt.tzinfo is None:
        dt = dt.replace(tzinfo=tz_in)
    else:
        raise Exception("Only timezone aware allowed")
    return dt.astimezone(tz_out)

@typechecked
def iterate_dates(start_date: date, end_date: date):
  """Iterates through all dates from start_date to end_date, inclusive.

  Args:
    start_date: A datetime object representing the start date.
    end_date: A datetime object representing the end date.

  Yields:
    A datetime object for each date in the range.
  """

  current_date = start_date
  while current_date <= end_date:
    yield current_date
    current_date += timedelta(days=1)

@typechecked
def datetime_to_short_rel_str(dt: datetime, vienna: bool = False, today: date | None = None) -> str:
    if today is None:
        today=date.today()
    tz = VIENNA if vienna else UTC
    dt = dt.astimezone(tz)
    diff = (dt.date() - today).days
    if diff == 0:
        formatted_date = "T 0"
    else:
        formatted_date = f"T{diff:+}"
    formatted_time = dt.strftime("%H:%M")
    return f"{formatted_date} {formatted_time}"

@typechecked
class MyDatetime(datetime):
    def to_short_rel_str(self, vienna: bool = False):
        return datetime_to_short_rel_str(self, vienna)



if __name__ == "__main__":
    for y in range(2015, 2018):
        for m in range (1, 12):
            dt = datetime(y, m, 1, 0, 0, 0, tzinfo=UTC)
            print(f"1.{m}.{y}: {get_epoch_from_datetime(dt, ms=True)}")
