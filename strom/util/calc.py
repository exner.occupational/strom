#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from operator import itemgetter
from typing import List, Tuple, Union
from datetime import date, datetime, timedelta
from typeguard import typechecked
from loguru import logger
from strom.model.market import SimpleMarketDataModel, SimpleMarketDataList
from strom.service.power_schedule import PowerSchedule
from strom.service.cover_schedule import CoverSchedule, Cover, CoverPos
from strom.util.action import Main, Boiler, Vent
from strom.util.epoch import iterate_dates
from strom.util.const import VIENNA, BUY_ANYWAY_THRESHOLD_MAIN, ANSCHLUSSLEISTUNG, LATITUDE, LONGITUDE
from strom.util.sunrise import Sun
from strom.util.const import NO_POWER_HOURS, ANY_POWER_HOURS

month_winter = [9, 10, 11, 12, 1, 2, 3, 4, 5]

dist_rules = { # hours in any slot, hours in slot 1, hours in slot 2
        1: (1, 0, 0),
        2: (2, 0, 0),
        3: (3, 0, 0),
        4: (2, 1, 1),
        5: (2, 1, 2),
        6: (3, 1, 2),
        7: (3, 2, 2),
        8: (4, 2, 2),
        9: (5, 2, 2),
        10: (4, 3, 3),
        11: (5, 3, 3),
        12: (6, 3, 3)}

"""
Zeitfenster beim Nachtstrom-Zähler
Mo - So
* 20:30 - 22:00
* 23:00 - 07:30
* variabel 2h zwischen 12:00 - 15:00

            14 15 16 17 18 19 20 21 22 23 00 01 02 03 04 05 06 07 08 09 10 11 12 13
            ??                vv XX    XX XX XX XX XX XX XX XX vv             ?? ??

idR.  :     XX                vv XX    XX XX XX XX XX XX XX XX vv                XX
Slot 1:                                XX XX XX XX XX XX XX XX vv
Slot 2:     XX                vv                                                 XX
"""

slot_hours_1 = [23, 0, 1, 2, 3, 4, 5, 6, 7]
#slot_hours_2 = [12, 13, 14, 20, 21]
#slot_hours_2 = [13, 14, 20] # Um 12 ist praktisch nie Nachtstrom verfügbar
slot_hours_2 = [12, 13, 14, 15, 16, 17, 18]

def _split_slot(market_data: list, slot_hours: list, slot_no: int):
    """
    move values from prices to slot if they are within slot_hours
    move values from prices to rest otherwise
    """
    rest, slot = [], []
    for x in market_data:
        slot.append(x) if x[0].hour in slot_hours and len(slot) < slot_no else rest.append(x)
    return  rest, slot

@typechecked
def calc_distribution(heating_time: int, simple_market_data_list: SimpleMarketDataList, buy_anyway_threshold: Union[float, None] = None) -> List[Tuple[datetime, float]]:
    """
    heating_time: hours to heat
    market_data: list of (hour, price) 

    1: order by price, move to result
    2: move range1

    return: list of hours + price, e. g [(hour0, 3.4), (hour3, 5.6), (hour16, 2.4)]
    """
    market_data = [(smd.start_dt, smd.marketprice) for smd in simple_market_data_list if smd.start_dt.hour in slot_hours_1 + slot_hours_2]
    #market_data = [(smd.start_dt, smd.marketprice) for smd in simple_market_data_list]
    market_data_sorted = sorted(market_data, key=itemgetter(1)) # sort market_data by price
    (rest_no, slot_no_1, slot_no_2) = dist_rules[heating_time]
    rest, slot_1 = _split_slot(market_data_sorted, slot_hours_1, slot_no_1)
    rest, slot_2 = _split_slot(rest, slot_hours_2, slot_no_2)
    slot_any = rest[:rest_no]
    rest = rest[rest_no:]

    buy_anyway = []
    if buy_anyway_threshold:
        buy_anyway = [item for item in rest if item[1] <= buy_anyway_threshold]

    distribution  = sorted(slot_1 + slot_2 + slot_any + buy_anyway, key=itemgetter(0))
    logger.debug(f"distribution: {distribution}")
    #avg_cost = sum(x[1] for x in distribution) / len(distribution)
    return distribution

@typechecked
def get_heating_times(d: date, kwh: Union[int, float], simple_market_data_list: SimpleMarketDataList) -> List[Tuple[datetime, float]]:
    """
    d      .. date
    kwh    .. needed power
    prices .. list of prices to select, e. g. [(0, 3.14), (1, 5.33), (2, 7.55), (3, 9.77), (4, 4.56), ...]
    heating_period:   0 kWh: 0h
    heating_period: 120 kWh: 6h
    additional 1h in any case (cellar, hot water)

    return: list of hours, e. g [0, 1, 2, 3, 16, 17] and mean cost
    """
    assert isinstance(d, date)
    heating_time = int(max(kwh / ANSCHLUSSLEISTUNG, 0) + 1 + 0.5) # lt. Modell + 1h zur Sicherheit + 0.5h zur kaufmännischen Rundung
    heating_time = max(heating_time, 2) # Boiler Winter wie Sommer
    heating_time = min(heating_time, 12) # Maximalwert aufgrund dist_rules
    return calc_distribution(heating_time, simple_market_data_list, buy_anyway_threshold=BUY_ANYWAY_THRESHOLD_MAIN)

@typechecked
def calc_main_schedule(main_schedule: PowerSchedule, cheap_hours: List[Tuple[datetime, float]], smdl: SimpleMarketDataList):
    """
    :cheap_hours: [[<hour>, <price>], [<hour>, <price>], ...]
    """
    main_schedule.add_interval(smdl.start_dt(), smdl.end_dt() - smdl.start_dt(), Main.OFF, override=True)
    for item in cheap_hours:
        if item[0].hour in ANY_POWER_HOURS:
            action = Main.ANY
        elif item[0].hour in NO_POWER_HOURS:
            action = Main.H1
        else:
            action = Main.NS
        main_schedule.add_interval(item[0], timedelta(hours=1), action, override=True)

        #if item[1] >= H1_PRICE:
        #    main_schedule.add_interval(item[0], timedelta(hours=1), Main.H1, override=True)
        #else:
        #    main_schedule.add_interval(item[0], timedelta(hours=1), Main.NS, override=True)

@typechecked
def calc_boiler_schedule(boiler_schedule: PowerSchedule, cheap_hours: List[Tuple[datetime, float]], smdl: SimpleMarketDataList):
    """
    :cheap_hours: [[<hour>, <price>], [<hour>, <price>], ...]
    """
    boiler_schedule.add_interval(smdl.start_dt(), smdl.end_dt() - smdl.start_dt(), Boiler.OFF, override=True)
    for item in cheap_hours:
        boiler_schedule.add_interval(item[0], timedelta(hours=1), Boiler.ON, override=True)

@typechecked
def calc_vent_schedule(vent_schedule: PowerSchedule, start_d: date, end_d: date):
    winter = True if start_d.month in month_winter else False
    for my_date in iterate_dates(start_d, end_d):
        my_start_dt = datetime(my_date.year, my_date.month, my_date.day, tzinfo=VIENNA)
        # Switch the vent the whole day on (default behaviour)
        vent_schedule.add_interval(my_start_dt, timedelta(days=1), Vent.ON, override=True)
        # Stop on Weekdays between 07:00 and 14:00
        if winter and (my_date.weekday() not in (5, 6)):
            vent_schedule.add_interval(my_start_dt + timedelta(hours=7), timedelta(hours=7), Vent.OFF, override=True)

@typechecked
def calc_cover_schedule(cover_schedule: CoverSchedule, start_d: date, end_d: date):
    for my_date in iterate_dates(start_d, end_d):
        my_dt = datetime(my_date.year, my_date.month, my_date.day, tzinfo=VIENNA)
        sun = Sun(my_date, LATITUDE, LONGITUDE)
        sunset_dt = sun.sunset() + timedelta(minutes=15) # close 30 minutes after sunset

        if my_date.weekday() not in (5, 6):
            # Weekdays
            # Wohnzimmer auf um 06:00
            cover_schedule.add_action(my_dt.replace(hour=6), Cover.WZ_WIN, CoverPos.OPEN)
            cover_schedule.add_action(my_dt.replace(hour=6), Cover.WZ_DOOR, CoverPos.OPEN)
            # Wohnzimmer zu um 07:30
            # Offen lassen wegen Pflanzen am Fensterbrett
            #cover_schedule.add_action(my_dt.replace(hour=7, minute=30), Cover.WZ_WIN, CoverPos.CLOSE)
            cover_schedule.add_action(my_dt.replace(hour=7, minute=30), Cover.WZ_DOOR, CoverPos.CLOSE)
            # Wohnzimmer auf um 14:00
            cover_schedule.add_action(my_dt.replace(hour=14), Cover.WZ_WIN, CoverPos.OPEN)
            cover_schedule.add_action(my_dt.replace(hour=14), Cover.WZ_DOOR, CoverPos.OPEN)
            # Schlafzimmer auf um 16:00
            cover_schedule.add_action(my_dt.replace(hour=16), Cover.SZ_DOOR, CoverPos.OPEN)
        else:
            # Weekend
            # Wohnzimmer auf um 07:00
            cover_schedule.add_action(my_dt.replace(hour=7), Cover.WZ_WIN, CoverPos.OPEN)
            cover_schedule.add_action(my_dt.replace(hour=7), Cover.WZ_DOOR, CoverPos.OPEN)
        # Any Day
        cover_schedule.add_action(sunset_dt, Cover.WZ_WIN, CoverPos.CLOSE)
        cover_schedule.add_action(sunset_dt, Cover.WZ_DOOR, CoverPos.CLOSE)
        cover_schedule.add_action(sunset_dt, Cover.SZ_DOOR, CoverPos.CLOSE)
