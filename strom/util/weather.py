#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from datetime import datetime, date, timedelta
import requests
#import enum
#from enum import StrEnum # Python 3.11 ?
from strenum import StrEnum
from loguru import logger
from typing import Dict
from typeguard import typechecked
from strom.service.db import DB, DBDomain
from strom.util.const import VIENNA
from strom.util.epoch import datetime_to_tzaware, get_epoch_from_datetime, get_epoch_from_date, get_datetime_from_epoch
from strom.model.weather import WeatherDayModel
#import json


# https://stackoverflow.com/questions/74884921/how-to-add-attributes-to-a-enum-strenum
class WeatherHourly(StrEnum):
 
    def __new__(cls, value, orig_name, label, color):
        member = str.__new__(cls, value)
        member._value_ = value
        member.orig_name = orig_name
        member.label = label
        member.color = color
        return member

    TEMPERATURE = "temperature", "temperature_2m", "Temperatur (°C)", "purple"
    RADIATION = "radiation", "shortwave_radiation", "Sonne (W/m²)", "orange"
    WINDSPEED = "windspeed", "windspeed_10m", "Wind (km/h)", "blue"

class WeatherDaily(StrEnum):
 
    def __new__(cls, value, label):
        member = str.__new__(cls, value)
        member._value_ = value
        member.label = label
        return member


    DAYLIGHT_HOURS = "daylight_hours", "Tageslänge (h)"
    SUNRISE = "sunrise", "Sonnenaufgang"
    SUNSET = "sunset", "Sonnenuntergang"


@typechecked
class Weather():

    def __init__(self):
        self.daily = None
        self.hourly = None

    @staticmethod
    def _get_dict(subj):
        """
        convert:
        {
          "a": ["val1", "val2"],
          "b": ["val3", "val4"],
          "time": ["key1", "key2"]
        }
        to:
        {
          "key1": {"a": "val1", "b": "val3"},
          "key2": {"a": "val2", "b": "val4"}
        }

        :return: e. g. on hourly sub-json:
        {
          '2023-03-10T00:00': {'shortwave_radiation': 0.0, 'windspeed_10m': 7.0, 'temperature_2m': 7.8},
          '2023-03-10T01:00': {'shortwave_radiation': 0.0, 'windspeed_10m': 8.2, 'temperature_2m': 8.7},
          '2023-03-10T02:00': {'shortwave_radiation': 0.0, 'windspeed_10m': 5.6, 'temperature_2m': 7.7},
          ...
        }
        e. g. on daily sub-json:
        {
          '2023-03-10': {'sunset': '2023-03-10T17:53', 'sunrise': '2023-03-10T06:17'}
        }
        """
        # pass e. g. hourly or daily from open-meteo response
        d = {}
        if not "time" in subj.keys():
            raise Exception("to time field available")
        for i, time in enumerate(subj["time"]):
            subd = {}
            for key in set(subj) - {"time"}:
                subd[key] = subj[key][i]
            d[time] = subd
        return d


    @staticmethod
    def _request_weather_data(start_d: date, end_d: date) -> dict:
        """
        build an request like
        https://api.open-meteo.com/v1/forecast?latitude=48.1742&longitude=16.2559&hourly=temperature_2m,windspeed_10m,shortwave_radiation&daily=sunrise,sunset&timezone=Europe%2FBerlin&start_date=2024-01-29&end_date=2024-01-29
        and return raw json API response
        DOES FIX tz unaware datetime fields to tz aware datetime
        :return: e. g.:
        {'latitude': 48.18, ...,
         'hourly_units': {'time': 'iso8601', ...},
         'hourly': {'time': ['2023-03-10T00:00', ...], 'temperature_2m': [7.8, ...], 'windspeed_10m': [7.0, ...], 'shortwave_radiation': [0.0, ...]},
         'daily_units': {'time': 'iso8601', ...},
         'daily': {'time': ['2023-03-10'], 'sunrise': ['2023-03-10T06:17'], 'sunset': ['2023-03-10T17:53']}}
        """
        start_d_iso = start_d.isoformat()
        end_d_iso = end_d.isoformat()
        params = {
            "latitude": "48.1742",
            "longitude": "16.2559",
            "hourly": ["temperature_2m", "windspeed_10m", "shortwave_radiation"],
            "daily": ["sunrise", "sunset"],
            "timezone": "Europe/Berlin",
            "start_date": start_d_iso,
            "end_date": end_d_iso
        }
        url = "https://api.open-meteo.com/v1/forecast"
        response = requests.get(url, params=params)
        sc = response.status_code
        logger.debug(f"{start_d_iso} - {end_d_iso}: Status Code {sc}")
        if sc != 200:
            # ToDo: return fallback from model without weather data
            raise Exception("Got no weather forecast")
        return response.json()

 
    @staticmethod
    def _post_process(raw) -> (dict, dict):
        """
        input: raw data from weather serivce
        output: timezone aware datetimes + reorganized data according to _get_dict()
        """
        # utc_offset_seconds = raw["utc_offset_seconds"] # ToDo: Use this information to convert to timezone aware datetimes
        raw["hourly"]["time"] = [datetime_to_tzaware(x, vienna_in=True, vienna_out=True).isoformat() for x in raw["hourly"]["time"]]
        raw["daily"]["sunrise"] = [datetime_to_tzaware(x, vienna_in=True, vienna_out=True).isoformat() for x in raw["daily"]["sunrise"]]
        raw["daily"]["sunset"] = [datetime_to_tzaware(x, vienna_in=True, vienna_out=True).isoformat() for x in raw["daily"]["sunset"]]
        return (Weather._get_dict(raw["daily"]), Weather._get_dict(raw["hourly"]))



    def dump(self, db: DB) -> None:
        for day, dic in self.daily.items():
            db.put(DBDomain.WEATHER_DAY, get_epoch_from_date(date.fromisoformat(day), ms=False, vienna=True), dic)
        for hour, dic in self.hourly.items():
            db.put(DBDomain.WEATHER_HOUR, get_epoch_from_datetime(datetime.fromisoformat(hour), ms=False), dic)


    def load(self, start_d: date, end_d: date, db: DB | None = None) -> None:
        if db:
            # ToDo: daily
            start = get_epoch_from_date(start_d, ms=False, vienna=True)
            stop = get_epoch_from_date(end_d + timedelta(days=1), ms=False, vienna=True)
            self.daily = {get_datetime_from_epoch(k, ms=False, vienna=True).date().isoformat(): v for k, v in db.get(DBDomain.WEATHER_DAY, start=start, stop=stop).items()}
            self.hourly = {get_datetime_from_epoch(k, ms=False, vienna=True).isoformat(): v for k, v in db.get(DBDomain.WEATHER_HOUR, start=start, stop=stop).items()}
        else:
            (self.daily, self.hourly) = Weather._post_process(Weather._request_weather_data(start_d, end_d))


    def get_weather_data(self, d: date) -> WeatherDayModel:
        """
        turn raw weather API json data to a dict of model input parameters.
        :return: e. g. {'temperature': 257.25, 'radiation': 3440.0, 'windspeed': 374.8, 'daylight_hours': 11.55}
        """
        d_iso = d.isoformat()
        #logger.debug(json.dumps(self.weather_json["hourly"], indent=4))
        hourly_fields = ["temperature_2m", "shortwave_radiation", "windspeed_10m"]
        my_keys = ["temperature", "radiation", "windspeed"]
        param_dict_keys = ["temperature", "radiation", "windspeed", "daylight_hours"]
        param_dict = {f: 0.0 for f in param_dict_keys}
        hourly = self.hourly
        #logger.debug(hourly)
        h = 0
        for k, v in {key: value for key, value in hourly.items() if key.startswith(d_iso)}.items():
            h += 1
            for i, field in enumerate(hourly_fields):
                param_dict[my_keys[i]] = param_dict[my_keys[i]] + v[field]
        # technically at least one hour is needed; more than 25 hours are suspicious
        if not 1 <= h <= 25:
            raise Exception("24 records expected but got {h}")
        if h != 24:
            logger.warn(f"not 24 but {h} records were delivered")
        # calculate average temperature, radiation and windspeed
        for k in my_keys:
            param_dict[k] = param_dict[k] / h

        #logger.debug(json.dumps(self.weather_json["daily"], indent=4))
        daily = self.daily
        #logger.debug(daily)
        sunrise = datetime.fromisoformat(daily[d_iso]["sunrise"])
        sunset = datetime.fromisoformat(daily[d_iso]["sunset"])
        daylight_hours = (sunset - sunrise).total_seconds() / 3600
        param_dict["daylight_hours"] = daylight_hours
        param_dict["day"] = d
        weather_day = WeatherDayModel.model_validate(param_dict)
        return weather_day


    def get_graph_weather_data(self) -> dict:
        """
        builds something like:
        {
          'time': [t1, t2, t2, t3, t3, t4, ...],
          'temperature_2m': [4.2, 4.2, 6.6, 6.6, 7.7, 7.7...],
          'shortwave_radiation': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ...],
          'windspeed_10m': [19.9, 19.9, 21.6, 21.6, 15.5, 15.5, ...]
        }
        """
        hourly_fields = ["temperature_2m", "shortwave_radiation", "windspeed_10m"]
        hourly = self.hourly
        prep_hourly = {}
        # span each time util next hour
        # [1, 2, 3] -> [1, 2, 2, 3, 3, 4]
        prep_hourly["time"] = [datetime.fromisoformat(h) + timedelta(hours = i) for h in hourly.keys() for i in range(2)]
        # each weather data point twice
        # [1, 2, 3] -> [1, 1, 2, 2, 3, 3]
        for field in hourly_fields:
            prep_hourly[field] = [dic[field] for dic in hourly.values() for i in range(2)]
        return prep_hourly




if __name__ == "__main__":
    today = date.today()
    tomorrow = today + timedelta(days=1)
    weather = Weather()
    weather.load(today, tomorrow, None)
    data = weather.get_weather_data(today)
    logger.debug(f"data of today ({today.isoformat()}):\n{data}")
    data = weather.get_weather_data(tomorrow)
    logger.debug(f"data of tomorrow ({tomorrow.isoformat()}):\n{data}")
    logger.debug(f"get_graph_weather_data: {weather.get_graph_weather_data()}")
