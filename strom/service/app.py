#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import gc
#from guppy import hpy
from flask import Flask, request, render_template, Response
from threading import Thread
import requests
import time
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from typing import List, Dict
from loguru import logger
import atexit
#from zoneinfo import ZoneInfo
#import io
import os
import psutil
import traceback
#import plyvel
from typeguard import typechecked
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from strom.util.const import VIENNA, UTC, BUY_ANYWAY_THRESHOLD_BOILER
from strom.util.epoch import get_midnight, get_epoch_from_datetime, get_datetime_from_epoch, vienna_now, epoch_now, epoch_add_timedelta
from strom.util.graph import create_prices, create_weather, create_avail, create_avail_state, create_avail_state_2
#from strom.util.market import request_market_data, get_market_data, get_chart_market_data, MarketHandler
from strom.util.market import MarketHandler
from strom.util.weather import Weather
from strom.util.calc import calc_main_schedule, calc_boiler_schedule, calc_vent_schedule, calc_cover_schedule, get_heating_times, calc_distribution
from strom.service.db import DB, DBDomain, db
from strom.service.control import RelayControl, CoverControl
from strom.util.action import ColorizedEnum, Avail, Main, Boiler, Vent
from strom.service.power_schedule import main_schedule, boiler_schedule, vent_schedule
from strom.service.cover_schedule import cover_schedule, Cover, CoverPos
from strom.service.forecast import eval_model
from strom.version import __version__ as version

from strom.util.const import TEMPLATE_FOLDER, PORT, H1_PRICE #, LOG_FILE

#class ExLogger:
#    def __init__(self):
#        self.tracked = set()
#    #@staticmethod
#    def _ex_to_str(self, ex: Exception):
#        return self.tracked.add(str(type(ex)) + "".join(traceback.format_tb(ex.__traceback__)))
#    def set_ex(self, ex: Exception):
#        #if ExLogger._ex_to_str(ex) in self.tracked:
#        if self._ex_to_str(ex) in self.tracked:
#            logger.error(ex)
#        else:
#            logger.exception(ex)
#            #self.tracked.add(ExTracker._ex_to_str(ex))
#            self.tracked.add(self._ex_to_str(ex))
#    def reset(self):
#        self.tracked = set()

class RecalcTracker:

    @staticmethod
    @typechecked
    def _get_applicable_datetime(vnow: datetime) -> datetime:
        logger.debug(vnow)
        if vnow.hour >= 14:
            return vnow + relativedelta(hour=14, minute=0, second=0, microsecond=0)
        else:
            return vnow + relativedelta(days=-1, hour=14, minute=0, second=0, microsecond=0)

    def __init__(self):
        self.last = datetime(1, 1, 1, 0, 0, tzinfo=VIENNA)

    @typechecked
    def needed(self, vnow: datetime) -> bool:
        """
        Informiert, ob eine Zeitplanung notwendig ist. Bei Eintritt in eine neue Periode ist sie jedenfalls notwendig.
        """
        next_date = RecalcTracker._get_applicable_datetime(vnow)
        logger.debug(f"check if last: {self.last} < next_date: {next_date}")
        return self.last < next_date

    @typechecked
    def done(self, vnow: datetime) -> None:
        """
        Setzt für die aktuelle Periode die Zeitplanung auf erledigt.
        """
        last = RecalcTracker._get_applicable_datetime(vnow)
        logger.debug(f"set last to {last}")
        self.last = last


#guppy_hpy = hpy()
#template_folder = '/home/pi/git/strom/templates'
#print(template_folder)
app = Flask(__name__, template_folder=TEMPLATE_FOLDER,)
process = psutil.Process(os.getpid())
relay_control = RelayControl()
cover_control = CoverControl()
switch_state = False
market_handler = MarketHandler()
smdl = None # SimpleMarketDataList
weather = Weather()
recalc_tracker = RecalcTracker()
kwh = 0

# Energiepreise "Nachtstrom Speicherheizung" ab 01.09.2022:
# 26.6675 inkl. 20% USt. und 6% Gebrauchsabgabe
# awattar bietet über das API Netto-Preise an.
# Daher 20% und 6% korrigieren sowie ca. 2 Cent weniger Nebenkosten abziehen.
# 26.6675 / 1.06 / 1.2 - 2 = 18.9650 Cent/kWh


@typechecked
def get_start_end_date(dt: datetime) -> (date, date):
    """
    Z. B.: 03.04.2023 14:18:32.324 -> 02.04.2023 03.04.2023
    Z. B.: 03.04.2023 15:25:41.657 -> 03.04.2023 04.04.2023
    """
    d1 = timedelta(days=-1 if dt.hour < 14 else 0)
    d2 = timedelta(days=1)
    return ((dt + d1).date(), (dt + d1 + d2).date())

@typechecked
def check_time(vnow: datetime):
    """
    Wird jede Minute aufgerufen
    Prüft ob die jetzige Stunde im Array ist und ruft ggf. die switch Funktion auf.
    Um 15:00 werden neue Daten vom API aufgerufen.
    """
    global market_handler
    global smdl
    global weather
    #global cheap_hours
    #global main_schedule
    global recalc_tracker
    global relay_control
    global cover_control
    global kwh

    (start_d, end_d) = get_start_end_date(vnow)
    logger.debug(f"start_d: {start_d}, end_d: {end_d}")

    # Delete old Actions from all PowerSchedules
    del_from = datetime.combine(start_d, datetime.min.time(), VIENNA) # convert start_d to datetime
    logger.debug(f"del_from: {del_from}")
    for schedule in ColorizedEnum.get_power_schedules():
        schedule.del_intervals_before(del_from)
    cover_schedule.del_actions_before(del_from)

    #logger.info(f"current time: {vnow} / epoch_start: {epoch_start} / epoch_end: {epoch_end}")
    if recalc_tracker.needed(vnow): # force recalculation of planned power switches
        try:
            logger.info("recalculation needed")
            # first calc Vent PowerSchedule because no input is needed
            calc_vent_schedule(vent_schedule, start_d, end_d)
            calc_cover_schedule(cover_schedule, start_d, end_d)

            # get data
            logger.info("get market data")
            market_handler.update(vnow)
            market_handler.dump(db)
            logger.info("get weather data")
            weather = Weather()
            weather.load(start_d, end_d + timedelta(days=3))
            weather.dump(db)
            # calc schedules
            weather_day = weather.get_weather_data(end_d) # input data of model to calculate kwh
            kwh = eval_model(weather_day)
            logger.debug(f"kWh: {kwh}")
            smdl = market_handler.get_simple_market_data_list(as_of=start_d, adjust=True) # set no power hours to H1_PRICE
            cheap_power_hours = get_heating_times(start_d, kwh, smdl)
            cheap_boiler_hours = calc_distribution(2, smdl, BUY_ANYWAY_THRESHOLD_BOILER) # Buy cheapest two hours
            logger.info(f"cheap power hours = {cheap_power_hours}")
            logger.info(f"cheap boiler hours = {cheap_boiler_hours}")
            """
            Zeitfenster beim Nachstrom-Zähler
            Mo - So
            * 20:30 - 22:00
            * 23:00 - 07:30
            * variabel 2h zwischen 12:00 - 15:00
            """
            logger.info("recalculating schedule")
            calc_main_schedule(main_schedule, cheap_power_hours, smdl)
            calc_boiler_schedule(boiler_schedule, cheap_boiler_hours, smdl)
            recalc_tracker.done(vnow)
            logger.info("recalculation done")
        except Exception as e:
            logger.exception(e)
            logger.error(e)

        """
        # Winterbetrieb
        # Tagstrom zwischen 17:00 und 17:30
        tmp_dt = vnow.replace(hour=17, minute=00, second=0, microsecond=0)
        main_schedule.add_interval(tmp_dt, timedelta(minutes=30), Main.H1, override=True)
        # Off zwischen nächsten Tag 00:00 und 04:00
        tmp_dt = vnow.replace(hour=0, minute=0, second=0, microsecond=0)
        main_schedule.add_interval(tmp_dt + timedelta(days=1), timedelta(hours=4), Main.OFF, override=True)
        # Off zwischen nächsten Tag 06:00 und 11:00
        tmp_dt = vnow.replace(hour=6, minute=0, second=0, microsecond=0)
        main_schedule.add_interval(tmp_dt + timedelta(days=1), timedelta(hours=5), Main.OFF, override=True)
        """

        """
        # Sommerbetrieb
        # Von heute 00:00 zwei Tage abschalten
        tmp_dt = vnow.replace(hour=0, minute=0, second=0, microsecond=0)
        main_schedule.add_interval(tmp_dt, timedelta(days=2), Main.OFF, override=True)
        # Gleicher Tag 20:30 - 21:00
        tmp_dt = vnow.replace(hour=20, minute=30, second=0, microsecond=0)
        main_schedule.add_interval(tmp_dt, timedelta(minutes=30), Main.NS, override=True)
        # Nächster Tag 05:30 und 06:00
        tmp_dt = vnow.replace(hour=5, minute=30, second=0, microsecond=0)
        main_schedule.add_interval(tmp_dt + timedelta(days=1), timedelta(minutes=30), Main.NS, override=True)
        """

    main_action = main_schedule.get_action(vnow)
    boiler_action = boiler_schedule.get_action(vnow)
    vent_action = vent_schedule.get_action(vnow)
    logger.debug(f"main action: {main_action} / boiler action: {boiler_action} / vent action: {vent_action}")
    relay_control.set_state(main_action)
    relay_control.set_state(boiler_action)
    relay_control.set_state(vent_action)
    for cover in Cover:
        action = cover_schedule.get_action(vnow, cover)
        if action:
            logger.debug(f"cover: {cover} -> action: {action}")
            cover_control.do_action(cover, action)
        else:
            logger.debug(f"No action for cover: {cover}")

def collect():
    logger.debug("Collecting...")
    n = gc.collect()
    #logger.debug(f"Number of unreachable objects collected by GC: {n}")
    #logger.debug(f"Uncollectable garbage: {gc.garbage}")

@typechecked
def _sleep_until_next_min() -> datetime:
    """
    Wartet bis zur nächsten vollen Minute und liefert diese anschließend zurück.
    """
    vnow = vienna_now()
    # Berechne die Zeit bis zur nächsten vollen Minute
    next_min = (vnow + timedelta(minutes=1)).replace(second=0, microsecond=0)
    time_to_sleep = (next_min - vnow).total_seconds()
    time.sleep(time_to_sleep)
    return next_min

def main_loop():
    #global logf
    global relay_control

    # for development to see the chart faster
    global market_handler
    try:
        market_handler.update(vienna_now())
        market_handler.dump(db)
    except:
        logger.warning("initial update of market data failed")

    #logger.debug(f"open file: {LOG_FILE}")
    #logf = open(LOG_FILE, "a+")

    while True:
        try:
            logger.info(f"start of main loop iteration, mem usage: {process.memory_info().rss}")
            vnow = _sleep_until_next_min()
            epoch = get_epoch_from_datetime(vnow, ms=False)
            #logger.info(guppy_hpy.heap())
            (avail, power, boiler, vent) = relay_control.get_state()
            db.put(DBDomain.AVAIL, epoch, avail)
            db.put(DBDomain.MAIN, epoch, power)
            db.put(DBDomain.BOILER, epoch, boiler)
            db.put(DBDomain.VENT, epoch, vent)
            #vnow_str = vnow.astimezone(VIENNA).strftime("%d.%m.%Y %H:%M")
            #logf.write(f"{vnow_str},{int(avail)}\n")
            #logf.flush()
            check_time(vnow)
            collect()
        except:
            logger.exception("main loop iteration went wrong. waiting 15 seconds and try again")
            time.sleep(15) # 15 seconds

def cleanup():
    #global logf
    # Release your allocated resources here
    logger.info("free resources")
    db.close()
    #logf.close()

def start_counter_thread():
    # Register the cleanup function
    atexit.register(cleanup)
    counter_thread = Thread(target=main_loop, daemon=True)
    counter_thread.start()

@app.route('/prices.png')
def prices_png():
    global market_handler
    #global main_schedule
    #global schedule_dict

    try:
        if market_handler:
            prices = market_handler.get_chart_market_data()
            date_list, price_list = zip(*prices)
            date_list = list(date_list)
            price_list = list(price_list)
            logger.debug(f"# dates: {len(date_list)} / # prices: {len(prices) / 2} / H1_PRICE: {H1_PRICE} / schedule: {main_schedule}")
            output_value = create_prices(date_list, price_list, H1_PRICE, vienna_now(), db)
            return Response(output_value, mimetype='image/png')
        else:
            return "price data not available"
    except Exception:
        logger.exception("Error on generating PNG of prices")

@app.route('/weather.png')
def weather_png():
    vnow = vienna_now()
    (start_d, end_d) = get_start_end_date(vnow)
    weather = Weather()
    weather.load(start_d, end_d, db)
    output_value = create_weather(weather, vnow)
    return Response(output_value, mimetype='image/png')
    #else:
    #    return "weather data not available"

@app.route('/state.html')
def state_html():
    global relay_control
    global kwh
    (avail, power, boiler, vent) = relay_control.get_state()
    #logger.debug(f"NS: {avail}")
    avail_str = avail.name if avail else "n/a"
    power_text = power.name if power else "n/a"
    boiler_text = boiler.name if boiler else "n/a"
    vent_text = vent.name if vent else "n/a"
    return f"<p>Schaltung Main: {power_text} Boiler: {boiler_text} Vent: {vent_text}/ Nachtstrom aktiv: {avail_str} / Mem: {process.memory_info().rss:,d} / kWh: {kwh} / Version: {version}</p>"

@app.route('/schedule.html', methods = ['GET'])
def schedule_html():
    schedule_type_name = request.args.get('schedule_type')
    if schedule_type_name == Cover.__name__:
        return render_template('cover_schedule.html', cover_schedule=cover_schedule)
    else:
        schedule_type = ColorizedEnum.from_str(schedule_type_name)
        schedule = schedule_type.get_power_schedule()
        logger.debug(f"power_schedule.html: {schedule}")
        return render_template('power_schedule.html', schedule=schedule)

@app.route('/schedule_add', methods = ['POST'])
def schedule_add():
    try:
        j = request.get_json()
        logger.debug(j)
        schedule_type_name = j["schedule_type"]
        if schedule_type_name == Cover.__name__:
            action = j["state"]
            start = datetime.fromisoformat(j["start"]).replace(tzinfo=VIENNA)
            cover_names = j["covers"]
            for cover_name in cover_names:
                cover_schedule.add_action(start, Cover[cover_name], CoverPos[action])
        else: 
            schedule_type = ColorizedEnum.from_str(schedule_type_name)
            action = schedule_type[j["state"]]
            power_schedule = schedule_type.get_power_schedule()
            start = datetime.fromisoformat(j["start"]).replace(tzinfo=VIENNA)
            duration = datetime.fromisoformat(j["end"]).replace(tzinfo=VIENNA) - start
            power_schedule.add_interval(start, duration, action, override=True)
            logger.debug(f"schedule_add: {power_schedule}")
        return "ok"
    except Exception as e:
        logger.error(str(e))
        #logger.exception(str(e))
        return str(e)

@app.route('/delete_action', methods = ['DELETE'])
def delete_action():
    schedule_type_name = request.args.get('schedule_type')
    if schedule_type_name == Cover.__name__:
        start_iso = request.args.get('start_iso')
        cover_name = request.args.get('cover')
        logger.debug(f"delete from {cover_schedule.get_schedule_name()} schedule: {start_iso} / {cover_name}")
        cover_schedule.del_action(datetime.fromisoformat(start_iso), Cover[cover_name])
    else:
        schedule_type = ColorizedEnum.from_str(schedule_type_name)
        start_iso = request.args.get('start_iso')
        schedule = schedule_type.get_power_schedule()
        logger.debug(f"delete from {schedule.get_schedule_name()} schedule: {start_iso}")
        schedule.del_interval(datetime.fromisoformat(start_iso))
    return "ok"

@app.route('/weather')
def get_weather():
    vnow = vienna_now()
    (start_d, end_d) = get_start_end_date(vnow)
    weather = Weather()
    weather.load(start_d, end_d, db)
    return {"daily": weather.daily, "hourly": weather.hourly}

@app.route('/smdl')
def get_smdl():
    global smdl
    return smdl.model_dump(mode='json')

@app.route('/')
@app.route('/schedules')
def get_schedules():
    return render_template('schedules.html', power_schedules=ColorizedEnum.get_power_schedules(), cover_schedule=cover_schedule)

@app.route('/profiles')
def get_profiles():
    return render_template('profiles.html')

@app.route('/switch')
def switch_switch():
    global switch_state
    switch_state = False if switch_state else True
    logger.debug(f"switch_state: {switch_state}")
    return 'ok'

@typechecked
def _get_db_data(days: int, domain: DBDomain) -> Dict[int, object]:
    """
    days: Number of days to return
    domain: DBDomain.AVAIL | DBDomain.STATE

    :returns: List of dict: epoch -> Avail | State
    """
    epoch_stop = epoch_now(ms=False)
    epoch_start = epoch_add_timedelta(epoch=epoch_stop, ms=False, td=timedelta(days=-days))
    logger.debug(f"get {domain.name} data, epoch_start: {epoch_start}, epoch_stop: {epoch_stop}")
    data_avail = db.get(domain, epoch_start, epoch_stop)
    logger.debug(f"{domain.name} size: {len(data_avail)}")
    return data_avail

@app.route('/avail')
def get_avail():
    days = int(request.args.get('days', 1))
    data_avail = _get_db_data(days, DBDomain.AVAIL)
    return {get_datetime_from_epoch(epoch=k, ms=False, vienna=True).isoformat(): v.name for k, v in data_avail.items()}

@app.route('/state')
def get_state():
    days = int(request.args.get('days', 1))
    data_state = _get_db_data(days, DBDomain.MAIN)
    return {get_datetime_from_epoch(epoch=k, ms=False, vienna=True).isoformat(): v.name for k, v in data_state.items()}

@app.route('/avail.png', methods = ['GET'])
def get_avail_png():
    days = int(request.args.get('days', 1))
    data_avail = _get_db_data(days, DBDomain.AVAIL)
    # keys from epoch to datetime array
    dt = []
    # avail data to array
    avail = []
    for k, v in data_avail.items():
        dt.append(get_datetime_from_epoch(epoch=k, ms=False, vienna=True))
        avail.append(v.value)
    output_value = create_avail(dt, avail)
    return Response(output_value, mimetype='image/png')

@app.route('/avail_state.png', methods = ['GET'])
def get_avail_state_png():
    days = int(request.args.get('days', 1))
    mode = int(request.args.get('mode', 1))
    data_avail = _get_db_data(days, DBDomain.AVAIL)
    data_state = _get_db_data(days, DBDomain.MAIN)
    if mode == 1:
        output_value = create_avail_state(data_avail, data_state)
    else:
        output_value = create_avail_state_2(data_avail, data_state)
    return Response(output_value, mimetype='image/png')



if __name__ == "__main__":
    try:
        start_counter_thread()
        app.debug = True
        app.run(host='0.0.0.0', port=PORT, processes=1, threaded=True, use_reloader=False)
    finally:
        relay_control = None
        logger.info("shutdown complete")
