#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from loguru import logger
from typing import List, Dict, Tuple, Union, Type
from datetime import datetime, timedelta
#from flask_table import Table, Col, DatetimeCol
import urllib.parse
from typing import TypeVar, Generic
from typeguard import typechecked
from strom.util.const import VIENNA #, UTC
from strom.util.market import SimpleMarketDataList
from strom.util.action import Main, Boiler, Vent #, ColorizedEnum
from strom.util.epoch import MyDatetime
#from strom.service.control import Main, Boiler, Vent #, ColorizedEnum

#TEnum = TypeVar('TEnum', bound=ColorizedEnum)
TEnum = TypeVar('TEnum', bound=Union[Main, Boiler, Vent])


@typechecked
class Planned(Generic[TEnum]):

    def __init__(self, duration: timedelta, action: TEnum) -> None:
        if duration <= timedelta(0):
            raise Exception("Duration MUST be positive.")
        self.duration = duration
        self.action = action

    def __str__(self) -> str:
        return f"{self.duration.total_seconds() / 3600}h: {self.action}"

@typechecked
class PowerSchedule(Generic[TEnum]):
    """
    Zur Steuerung, wann im Tagesverlauf Nachtstrom oder Tagstrom oder kein Strom bezogen werden soll.
    """
    def __init__(self, t: Type[TEnum]):
        t._schedule = self # register schedule at enum (see ColorizedEnum.get_power_schedule())
        self.t = t # to check function parameter
        self.intervals: Dict[datetime, Planned[TEnum]] = {}

    def _intervals_between(self, dt1: datetime, dt2: datetime) -> (List[datetime], List[datetime]):
        """
        Liefert alle Keys für Intervalle deren Start- oder End-Zeitpunkte zwischen dt1 und dt2 liegt.

        :return: Liste aller Keys wo Start im Intervall, Liste aller Keys wo Ende im Intervall
        """
        starts = [dt for dt in self.intervals.keys() if dt1 <= dt < dt2]
        ends = [dt for dt in self.intervals.keys() if dt1 < (dt + self.intervals[dt].duration) <= dt2]
        return (starts, ends)

    def _sort_join_intervals(self) -> None:
        """
        Join subsequent intervals of the same kind.
        """

        joined_intervals: Dict[datetime, Planned[TEnum]] = {}
        current_start = None

        for dt, planned in sorted(self.intervals.items(), key=lambda x: x[0]):
            if current_start is None:
                current_start = dt
                current_action = planned.action
                current_end = dt + planned.duration
            elif current_end == dt and current_action == planned.action:
                # The current interval starts at the previous ones end: extend the end time
                current_end = dt + planned.duration
            else:
                # There's a gap between intervals; add the previous interval to the result and start over
                joined_intervals[current_start] = Planned[TEnum](action=current_action, duration=current_end - current_start)
                current_start = dt
                current_action = planned.action
                current_end = dt + planned.duration

        # Add the last interval to the result
        joined_intervals[current_start] = Planned[TEnum](action=current_action, duration=current_end - current_start)
        # Memorize result
        self.intervals = joined_intervals

    def _del_intervals(self, to_remove: List[datetime]) -> None:
        """
        Delete intervals as requested by parameter

        :to_remove: List of datetimes/keys to remove
        """
        for k in to_remove:
            if k in self.intervals:
                del self.intervals[k]
            else:
                logger.error(f"key {k} could not be removed from intevals")

    def del_intervals_before(self, start: datetime) -> None:
        """
        Delete all intervals before start

        :start: All intervals which end before this datetime will be deleted
        """
        na = len(self.intervals)
        self.intervals = {dt: planned for dt, planned in self.intervals.items() if (dt + planned.duration) >= start}
        nb = len(self.intervals)
        logger.debug(f"deleted {na - nb} intervals")

    def del_interval(self, start: datetime) -> None:
        """
        Delete one interval

        :start: datetime of the interval to delete
        """
        self._del_intervals([start])

    def add_interval(self, dt: datetime, duration: timedelta, action: TEnum, override: bool) -> None:
        """
        Add one interval, sort intervals afterwards

        :dt: datetime, start of the interval
        :duration: timedelta, duration of the interval
        :action: what Action to do during the interval
        :override:
            If True, the interval will eventually override other intervals.
            If False, an exception will be raised when the interval overlaps with an existing interval.
        """
        assert type(action) == self.t
        #logger.debug(f"dt: {dt} / duration: {duration} / self: {str(self)}")
        logger.debug(f"dt: {dt} / duration: {duration}")
        (start_keys, end_keys) = self._intervals_between(dt, dt + duration)
        logger.debug(f"# overlapping starts / ends: {len(start_keys)} / {len(end_keys)}")
        if not override:
            if len(start_keys) == 0 and len(end_keys) == 0:
                self.intervals[dt] = Planned[TEnum](duration, action)
                #self.intervals[dt] = (duration, action)
            else:
                raise Exception("interval already planned")
        else:
            dt1 = dt
            dt2 = dt + duration
            # alle Intervalle löschen, die komplett zwischen dt und dt + duration liegen
            keys = [i for i in self.intervals.keys() if dt1 <= i <= dt2 and dt1 <= (i + self.intervals[i].duration) <= dt2]
            self._del_intervals(keys)
            # alle Intervalle am Ende abschneiden, wo das Ende zwischen dt und dt + duration liegt
            keys = [i for i in self.intervals.keys() if dt1 < (i + self.intervals[i].duration) <= dt2]
            for k in keys:
                self.intervals[k] = Planned[TEnum](dt1 - k, self.intervals[k].action)
            # alle Intervalle am Anfang abschneiden, wo der Beginn zwischen dt und dt + duration liegt
            keys = [i for i in self.intervals.keys() if dt1 <= i < dt2]
            for k in keys:
                self.intervals[dt2] = Planned[TEnum](k + self.intervals[k].duration - dt2, self.intervals[k].action)
                self.del_interval(k)
            # alle Intervalle teilen, deren Ende und Anfang über dt und dt + duration hinausgehen
            keys = [i for i in self.intervals.keys() if i < dt1 and (i + self.intervals[i].duration) > dt2]
            for k in keys:
                self.intervals[dt2] = Planned[TEnum](k + self.intervals[k].duration - dt2, self.intervals[k].action)
                self.intervals[k] = Planned[TEnum](dt1 - k, self.intervals[k].action)
            # finally add new interval
            self.intervals[dt] = Planned[TEnum](duration, action)
        self._sort_join_intervals()

    #def get_intervals(self, action: Main) -> List[Tuple[datetime, datetime]]:
    def get_intervals(self, action: TEnum, start_dt: datetime, end_dt: datetime) -> List[Tuple[datetime, timedelta]]:
        """
        Generate a sorted list of intervals of some planned action

        :action: result is filtered by action
        :return: sorted list of tuples (start, duration)
        """
        assert type(action) == self.t
        # No sort should be needed as self.intervals should be sorted
        #intervals = dict(sorted(self.intervals.items()))
        #intervals = [(k, v) for k, v in self.intervals.items() if k >= start_dt]
        #return [(k, v.duration) for k, v in intervals.items() if v.action == action]
        result = []
        for dt, value in self.intervals.items():
            if value.action == action:
                td = value.duration
                if dt >= start_dt and dt + td <= end_dt:
                    # The entire tuple is within the range
                    result.append((dt, td))
                elif dt < start_dt and dt + td >= end_dt:
                    # The tuple starts befor the range and extends beyond it
                    result.append((start_dt, end_dt - start_dt))
                elif dt < start_dt < dt + td:
                    # The tuple starts before the range but continues into it
                    truncated_td = dt + td - start_dt
                    result.append((start_dt, truncated_td))
                elif dt < end_dt <= dt + td:
                    # The tuple starts within the range but extends beyond it
                    truncated_td = end_dt - dt
                    result.append((dt, truncated_td))
                else:
                    print(f"dropping {dt}, {td}")
        return result

    def get_my_intervals(self) -> Dict[MyDatetime, Planned[TEnum]]:
       return {MyDatetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, tzinfo=dt.tzinfo): planned for dt, planned in self.intervals.items()}

    def get_action(self, dt: datetime) -> TEnum:
        """
        Gibt den RelayControl-Zustand zurück, der für den Zeitpunkt definiert ist.
        """
        keys = [i for i in self.intervals.keys() if i <= dt]
        if len(keys) > 0:
            key = max(keys)
            if key <= dt < key + self.intervals[key].duration:
                return self.intervals[key].action
        return self.t.NONE

    def get_schedule_name(self) -> str:
        return self.t.__name__

    def get_action_names(self) -> List[str]:
        return [action.name for action in self.t]

    def __str__(self) -> str:
        #return f"Type: {self.t.__name__}\n" + "\n".join([f"{k.strftime('%y%m%dT%H%M')}-{(k + v.duration).strftime('%y%m%dT%H%M')}:{v.action}" for k, v in self.intervals.items()])
        return f"Type: {self.t.__name__}\n" + "\n".join([f"{k.isoformat()} - {(k + v.duration).isoformat()}: {v.action}" for k, v in self.intervals.items()])

main_schedule = PowerSchedule(Main)
boiler_schedule = PowerSchedule(Boiler)
vent_schedule = PowerSchedule(Vent)
