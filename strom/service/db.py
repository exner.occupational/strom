#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from plyvel import DB as plyvelDB
#from plyvel._plyvel import PrefixedDB
import pickle
from loguru import logger
from typing import List, Dict, Union, Tuple, Any, Generator
from pydantic import BaseModel
from enum import Enum
from typeguard import typechecked
from strom.util.const import DB_FILE
from strom.util.epoch import vienna_now, get_epoch_from_datetime, assert_epoch, get_datetime_from_epoch
from strom.util.action import ColorizedEnum, Main, Avail
import traceback
#from deprecated import deprecated


TENDAYS = relativedelta(days=-10, hour=0, minute=0, second=0, microsecond=0)

class DBDomain(Enum):
    AVAIL = b'a'
    ACTION = b'p' # Ex Plan
    MAIN = b's'
    BOILER = b'b'
    VENT = b'v'
    WEATHER_DAY = b'd'
    WEATHER_HOUR = b'h'
    PRICE = b'm' # Money

dbdomain_to_class = {DBDomain.AVAIL: Avail, DBDomain.MAIN: Main}


@typechecked
def _key_to_str(key: bytes) -> str:
    return f"{key[:1].decode()} + {DB._decode_epoch(key[1:])}"


@typechecked
class DB():
    def __init__(self):
        logger.info(f"Open or create a LevelDB database: {DB_FILE}")
        self.db = plyvelDB(DB_FILE, create_if_missing=True)

    @staticmethod
    def _encode_epoch(epoch: int) -> bytes:
        encoded_epoch = epoch.to_bytes(8, 'big')
        return encoded_epoch

    @staticmethod
    def _encode_key(dbdomain: DBDomain, epoch: int) -> bytes:
        """
        The key is composed of the DBDomain enum value and the epoch encoded as 8 bytes.

        :dbdomain: e. g. DBDomain.AVAIL
        :epoch: Unix epoch (no milis)
        :return: database key
        """
        encoded_key = dbdomain.value + DB._encode_epoch(epoch)
        return encoded_key

    @staticmethod
    def _decode_epoch(serialized_epoch: bytes) -> int:
        """
        Convert the encoded epoch to an integer.

        :key: bytes encoded epoch
        :return: epoch as integer
        """
        return int.from_bytes(serialized_epoch, 'big')

    @staticmethod
    def _value_to_bytes(dbdomain: DBDomain, value: object) -> bytes:
        if dbdomain in (DBDomain.AVAIL, DBDomain.ACTION, DBDomain.MAIN, DBDomain.BOILER, DBDomain.VENT):
            assert isinstance(value, ColorizedEnum) or value is None, f"type(value): {type(value)}"
            if value is not None:
                serialized_value = bytes([value.value]) # Convert integer enum value to a single byte
            else:
                serialized_value = b''
        elif dbdomain in (DBDomain.WEATHER_DAY, DBDomain.WEATHER_HOUR):
            assert isinstance(value, dict), f"type(value): {type(value)}"
            serialized_value = pickle.dumps(value) # Convert dict to bytes
        elif dbdomain == DBDomain.PRICE:
            assert isinstance(value, float), f"type(value): {type(value)}"
            serialized_value = pickle.dumps(value) # Convert float to bytes
        else:
            raise Exception("not implemented")
        return serialized_value

    @staticmethod
    def _bytes_to_value(dbdomain: DBDomain, serialized_value: bytes) -> object:
        if dbdomain in (DBDomain.ACTION, DBDomain.MAIN, DBDomain.AVAIL):
            try:
                value = dbdomain_to_class[dbdomain](int.from_bytes(serialized_value, byteorder='big')) # Convert single byte to integer to enum
            except KeyError:
                raise Exception("ColorizedEnum class not implemented")
        elif dbdomain in (DBDomain.WEATHER_DAY, DBDomain.WEATHER_HOUR):
            value = pickle.loads(serialized_value)
        elif dbdomain == DBDomain.PRICE:
            value = pickle.loads(serialized_value)
        else:
            raise Exception("not implemented")
        return value

    def get_key_less_or_equal(self, dbdomain: DBDomain, start: int) -> bytes:
        """
        Get the DB key of dbdomain + start. If not exists return one before.
        """
        pdb = self.db.prefixed_db(dbdomain.value)
        key_start = DB._encode_epoch(start)
        # if some value is returned, the key exists
        if pdb.get(key_start):
            return DB._encode_key(dbdomain, start)
        # otherwise lookup the key previous to key_start
        #with self.db.iterator(start=key_start, reverse=True, include_value=False) as iterator:
        with pdb.iterator(reverse=True, include_value=False) as iterator:
            iterator.seek(key_start)
            return DB._encode_key(dbdomain, DB._decode_epoch(next(iterator)))

    def put(self, dbdomain: DBDomain, epoch: int, value):
        """
        key has to be a Unix epoch
        value has to be boolean
        """
        assert_epoch(epoch, ms=False)
        serialized_key = DB._encode_key(dbdomain, epoch)
        serialized_value = DB._value_to_bytes(dbdomain, value)
        self.db.put(serialized_key, serialized_value)

    def get(self, dbdomain: DBDomain, start: Union[int, None] = None, stop: Union[int, None] = None) -> Dict[int, object]:
        pdb = self.db.prefixed_db(dbdomain.value)
        key_start = DB._encode_epoch(start) if start else None
        key_stop = DB._encode_epoch(stop) if stop else None
        prefix = None if start else dbdomain.value

        result = dict()
        # Iterate through the keys and values
        #with pdb.iterator(start=key_start, include_start=True, stop=key_stop, include_stop=True) as db_it:
        with pdb.iterator(start=key_start, stop=key_stop) as db_it:
            for serialized_key, serialized_value in db_it:
                key = DB._decode_epoch(serialized_key)
                result[key] = DB._bytes_to_value(dbdomain, serialized_value)
            return result

    # Generator[yield_type, send_type, return_type]
    @typechecked
    def get_generator(self, dbdomain: DBDomain, start: Union[int, None] = None, stop: Union[int, None] = None) -> Generator[Tuple[int, Any], None, None]:
        pdb = self.db.prefixed_db(dbdomain.value)
        key_start = DB._encode_epoch(start) if start else None
        key_stop = DB._encode_epoch(stop) if stop else None
        prefix = None if start else dbdomain.value

        result = dict()
        # Iterate through the keys and values
        #with pdb.iterator(start=key_start, include_start=True, stop=key_stop, include_stop=True) as db_it:
        with pdb.iterator(start=key_start, stop=key_stop) as db_it:
            for serialized_key, serialized_value in db_it:
                key = DB._decode_epoch(serialized_key)
                value = DB._bytes_to_value(dbdomain, serialized_value)
                yield (key, value)

    def get_intervals(self, dbdomain: DBDomain, selected_value: object, start_dt: datetime, stop_dt: datetime) -> List[Tuple[datetime, timedelta]]:
        """
        Process database values like:
        <domain>timestamp 1: value x
        <domain>timestamp 2: value y
        <domain>timestamp 3: value x
        ...
        <domain>timestamp n: value x

        Process all values of the given domain and value within the given time range.
        Transforming consecutive same values into start datetime and length as timedelta.

        :dbdomain: Domain to process
        :obj: Value to select
        :return: list of (datetime, timedelta): Each tuple represents a start and length for consecutive values matching the specified value.
        """
        pdb = self.db.prefixed_db(dbdomain.value)
        start_epoch = get_epoch_from_datetime(start_dt, ms=False)
        stop_epoch = get_epoch_from_datetime(stop_dt, ms=False)
        key_start = self.get_key_less_or_equal(dbdomain, start_epoch)[1:]
        key_stop = self._encode_epoch(stop_epoch)
        early_end = True
        # Test if key_stop is the last key
        try:
            iterator = pdb.iterator(start=key_stop)
            test = next(iterator)
            early_end = False
        except:
            pass
        serialized_value = DB._value_to_bytes(dbdomain, selected_value)

        result = []
        current_start = None
        current_value = None
        with pdb.iterator(start=key_start, stop=key_stop) as db_it:
            for key, value in db_it:
                epoch = DB._decode_epoch(key)
                if value == serialized_value:
                    if current_value is None:
                        current_start = get_datetime_from_epoch(epoch, ms=False, vienna=True)
                    current_value = value
                elif current_value is not None:
                    end_time = get_datetime_from_epoch(epoch, ms=False, vienna=True)
                    interval_length = end_time - current_start
                    result.append((current_start, interval_length))
                    current_value = None
            if current_value is not None:
                if early_end:
                    end_time = vienna_now()
                else:
                    end_time = stop_dt
                interval_length = end_time - current_start
                result.append((current_start, interval_length))
        return result

    def get_intervals_strict(self, dbdomain: DBDomain, selected_value: object, start_dt: datetime, stop_dt: datetime) -> List[Tuple[datetime, timedelta]]:
        """
        Process database values like:
        <domain>timestamp 1: value x
        <domain>timestamp 2: value y
        <domain>timestamp 3: value x
        ...
        <domain>timestamp n: value x

        Process all values of the given domain and value within the given time range.
        Transforming consecutive same values into start datetime and length as timedelta.

        :dbdomain: Domain to process
        :obj: Value to select
        :return: list of (datetime, timedelta): Each tuple represents a start and length for consecutive values matching the specified value.
        """

        pdb = self.db.prefixed_db(dbdomain.value)

        current_start = None
        current_value = False
        result = []

        with pdb.iterator() as db_it:
            current_dt = start_dt
            while current_dt < stop_dt:
                current_epoch = get_epoch_from_datetime(current_dt, ms=False)
                current_key = self._encode_epoch(current_epoch)
                #print(pdb.get(current_key), selected_value)
                if (pdb.get(current_key) or bytes([0])) == DB._value_to_bytes(dbdomain, selected_value):
                    if not current_value:
                        current_start = current_dt
                    current_value = True
                elif current_value:
                    result.append((current_start, current_dt - current_start))
                    current_value = False
                current_dt += timedelta(minutes=1)

            if current_value:
                result.append((current_start, current_dt - current_start))

        return result

    def truncate(self, dt_delta: relativedelta = TENDAYS, epoch: Union[int, None] = None) -> int:
        """
        Remove older values that specified by dt_delta

        :dt_delta: timedelta
        :return: count of deleted records
        """
        count = 0
        if epoch is None:
            vnow = vienna_now()
            stop_epoch = get_epoch_from_datetime(vnow + dt_delta, ms=False)
        else:
            stop_epoch = epoch
        assert_epoch(stop_epoch, ms=False)
        for dbdomain in DBDomain:
            start = DB._encode_key(dbdomain, 0)
            stop = DB._encode_key(dbdomain, stop_epoch)
            #print(f"delete from {_key_to_str(start)} to {_key_to_str(stop)}")
            with self.db.iterator(start=start, stop=stop, include_value=False) as db_it:
                for serialized_key in db_it:
                    #print(f"delete key: {_key_to_str(serialized_key)}")
                    self.db.delete(serialized_key)
                    count += 1
        return count

    def __str__(self) -> str:
        result = "DB:\n"
        for key, value in self.db:
            result = result + f"{_key_to_str(key)}: {value}\n"
        return result


    def close(self):
        logger.info(f"Close LevelDB database: {DB_FILE}")
        try:
            self.db.close()
        except:
            pass

    #def __del__(self):
    #    logger.info(f"Close LevelDB database: {DB_FILE}")
    #    if hasattr(self, 'db'): # workaround
    #        self.db.close()

db = DB()
