#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

try:
    import RPi.GPIO as GPIO
except:
    import Mock.GPIO as GPIO
from typeguard import typechecked
from time import sleep
import requests
from enum import Enum
from typing import Tuple
from loguru import logger
from strom.util.action import Avail, Main, Boiler, Vent #, ColorizedEnum
from strom.service.cover_schedule import Cover, CoverPos


@typechecked
class CoverControl():
    cover_url_dict = {
        Cover.WZ_DOOR: "http://10.0.0.103/roller",
        Cover.WZ_WIN: "http://10.0.0.104/roller",
        Cover.SZ_DOOR: "http://10.0.0.105/roller"
    }

    def do_action(self, cover: Cover, action: int | CoverPos):
        base_url = CoverControl.cover_url_dict[cover]
        if isinstance(action, int):
            logger.info(f"drive cover to pos: {action} on url: {url}")
        else:
            match action:
                case CoverPos.OPEN:
                    url= f"{base_url}/0?go=open"
                case CoverPos.CLOSE:
                    url= f"{base_url}/0?go=close"
                case CoverPos.SLIT:
                    url=f"{base_url}/0?go=to_pos&roller_pos=15"
            try:
                logger.debug(f"Request: {url}")
                requests.get(url)
            except Exception:
                logger.error("Error on Cover REST call")

@typechecked
class RelayControl():
    H1 = 23 # Tagstrom Relais (Tagstrom an, wenn Relais an)
    NS = 25 # Nachtstrom Relais (Nachtstrom an, wenn Relais aus)
    BO = 5 # Boiler Relais (Hängt am Nachtstrom)
    VE = 6 # Ventilator Relais (Hängt am Tagstrom)
    IN = 13 # Eingang um zu messen, ob Nachtstrom vorhanden

    def __init__(self):
        #GPIO.cleanup()
        GPIO.setmode(GPIO.BCM) # choose BCM or BOARD
        GPIO.setup(RelayControl.H1, GPIO.OUT, initial=GPIO.LOW) # LOW .. Tagstrom aus
        GPIO.setup(RelayControl.NS, GPIO.OUT, initial=GPIO.LOW) # LOW .. Nachtstrom ein
        GPIO.setup(RelayControl.BO, GPIO.OUT, initial=GPIO.LOW) # LOW .. Boiler aus
        GPIO.setup(RelayControl.VE, GPIO.OUT, initial=GPIO.LOW) # LOW .. Ventilator aus
        GPIO.setup(RelayControl.IN, GPIO.IN) # do not configure pull up / pull down
        #GPIO.setup(RelayControl.IN, GPIO.IN, pull_up_down = GPIO.PUD_DOWN) # does not work?

    def __del__(self):
        self.power_ns()
        GPIO.cleanup()

    def power_h1(self):
        GPIO.output(RelayControl.NS, GPIO.HIGH) # Nachtstrom aus
        sleep(0.1)
        GPIO.output(RelayControl.H1, GPIO.HIGH) # Tagstrom ein

    def power_ns(self):
        GPIO.output(RelayControl.H1, GPIO.LOW) # Tagstrom aus
        sleep(0.1)
        GPIO.output(RelayControl.NS, GPIO.LOW) # Nachtstrom ein

    def power_off(self):
        GPIO.output(RelayControl.NS, GPIO.HIGH) # Nachtstrom aus
        GPIO.output(RelayControl.H1, GPIO.LOW) # Tagstrom aus

    def is_ns_avail(self) -> Avail:
        return Avail.ON if (GPIO.input(RelayControl.IN) != GPIO.HIGH) else Avail.OFF

    def boiler_on(self):
        GPIO.output(RelayControl.BO, GPIO.HIGH) # Boiler ein

    def boiler_off(self):
        GPIO.output(RelayControl.BO, GPIO.LOW) # Boiler aus

    def vent_on(self):
        GPIO.output(RelayControl.VE, GPIO.HIGH) # Ventilator ein

    def vent_off(self):
        GPIO.output(RelayControl.VE, GPIO.LOW) # Ventilator aus

    def set_state(self, action_enum: Main | Boiler | Vent) -> None:
        #logger.debug(action_enum)
        if isinstance(action_enum, Main):
            if action_enum == Main.OFF:
                self.power_off()
            elif action_enum == Main.NS:
                self.power_ns()
            elif action_enum == Main.H1:
                self.power_h1()
            elif action_enum == Main.ANY:
                if self.is_ns_avail() == Avail.ON:
                    self.power_ns()
                else:
                    self.power_h1()
            else:
                logger.warning("undefined Main state -> set NS")
                self.power_ns()
        elif isinstance(action_enum, Boiler):
            if action_enum == Boiler.OFF:
                self.boiler_off()
            elif action_enum == Boiler.ON:
                self.boiler_on()
            else:
                logger.warning("undefined Boiler state -> set ON")
                self.boiler_on()
        elif isinstance(action_enum, Vent):
            if action_enum == Vent.OFF:
                self.vent_off()
            elif action_enum == Vent.ON:
                self.vent_on()
            else:
                logger.warning("undefined Vent state -> set ON")
                self.vent_on()
        else:
            logger.warning("undefined action type")

    def get_state(self) -> Tuple[Avail, Main, Boiler, Vent]:
        """
        Provides information about Raspberry Pi Input and State
        :returns: Tuple of: Verfügbarkeit Nachtstrom, Schaltzustand von Hauptrelais, Boiler und Ventilator
        """
        h1_state = GPIO.input(RelayControl.H1)
        ns_state = GPIO.input(RelayControl.NS)
        bo_state = GPIO.input(RelayControl.BO)
        ve_state = GPIO.input(RelayControl.VE)
        # ToDo (hardware + software): Check if Nachtstrom is available
        avail = self.is_ns_avail()
        power = Main.FAULTY
        if h1_state == GPIO.LOW and ns_state == GPIO.LOW: power = Main.NS
        if h1_state == GPIO.HIGH and ns_state == GPIO.HIGH: power = Main.H1
        if h1_state == GPIO.LOW and ns_state == GPIO.HIGH: power = Main.OFF
        boiler = Boiler.NONE
        if bo_state == GPIO.LOW: boiler = Boiler.OFF
        if bo_state == GPIO.HIGH: boiler = Boiler.ON
        vent = Vent.NONE
        if ve_state == GPIO.LOW: vent = Vent.OFF
        if ve_state == GPIO.HIGH: vent = Vent.ON
        return (avail, power, boiler, vent)
