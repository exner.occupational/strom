#
# Copyright (c) 2024 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from flask import Flask, request, render_template, Response
from threading import Thread
import requests
import time
import os
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from loguru import logger
import atexit
from typeguard import typechecked
from multiprocessing import Manager
from strom.util.epoch import vienna_now
import random

# Generate a random integer between 1 and 10 (inclusive)
bar = None
foobar = 1

manager = Manager()
store = manager.dict()

app = Flask(__name__)


@typechecked
def check_time(vnow: datetime):
    """
    Wird jede Minute aufgerufen
    Prüft ob die jetztige Stunde im Array ist und ruft ggf die switch funktion auf
    um 15:00 werden neue Daten vom API aufgerufen
    """
    global foobar
    foobar = foobar + 1
    store["foo"] = datetime.now().isoformat()

@typechecked
def _sleep_until_next_5_sec() -> datetime:
    """
    Wartet bis zur nächsten vollen Minute und liefert diese anschließend zurück.
    """
    vnow = vienna_now()
    # Berechne die Zeit bis zur nächsten vollen Minute
    next_min = (vnow + timedelta(seconds=5)).replace(microsecond=0)
    time_to_sleep = (next_min - vnow).total_seconds()
    time.sleep(time_to_sleep)
    return next_min

def main_loop():

    while True:
        try:
            logger.info(f"start of main loop iteration")
            vnow = _sleep_until_next_5_sec()
            check_time(vnow)
        except:
            logger.exception("main loop iteration went wrong. waiting 15 seconds and try again")
            time.sleep(5) # 5 seconds

def cleanup():
    logger.info("free resources")

def start_counter_thread():
    # Register the cleanup function
    atexit.register(cleanup)
    counter_thread = Thread(target=main_loop, daemon=True)
    counter_thread.start()


@app.route('/test.json')
def get_weather():
    #worker_pid = os.getpid()
    time.sleep(1)
    global foobar
    global bar
    if bar is None:
        bar = random.randint(1, 100)
        logger.info(f"created bar: {bar}")
    foobar = 77
    foo = store["foo"]
    return {"foo": foo, "bar": bar, "foobar": foobar}



if __name__ == "__main__":
    logger.info("wrong place")
    #try:
    #    start_counter_thread()
    #    app.debug = True
    #    app.run(host='0.0.0.0', port=PORT, processes=1, threaded=False, use_reloader=False)
    #finally:
    #    relay_control = None
    #    logger.info("shutdown message")
