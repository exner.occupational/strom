#
# Copyright (c) 2023 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from datetime import datetime, date, timedelta
from typing import List, Tuple, Union
from typeguard import typechecked
from operator import itemgetter
import requests
import json
from strom.model.market import SimpleMarketDataModel, SimpleMarketDataList
from strom.model.weather import WeatherDayModel

# Modell im Jahr 2023; Basis: Aggregierte Daten lt. manueller Stromablesung
#model_dict = {'temperature': -77.20352511166335, 'radiation': 2.7808968681494264, 'windspeed': 5.578920054492248, 'daylight_hours': -11.8095589532916, 'const': 115.56444552270315, 'min': {'temperature': -1.4917317708333335, 'radiation': 41.608539094650205, 'windspeed': 8.141008771929824, 'daylight_hours': 8.49654761904762}, 'max': {'temperature': 15.310912698412695, 'radiation': 228.96929824561403, 'windspeed': 16.41022012578616, 'daylight_hours': 13.581228070175436}}

# Basis: 2023er Daten ohne Sonneneinstrahlung
model_dict = {'temperature': -65.12481950410402, 'windspeed': 25.86281670907238, 'daylight_hours': -28.633331845017842, 'const': 86.55481398653154, 'min': {'temperature': -7.808333333333334, 'windspeed': 4.4125000000000005, 'daylight_hours': 8.333333333333334}, 'max': {'temperature': 18.520833333333332, 'windspeed': 35.21666666666667, 'daylight_hours': 14.383333333333333}}

#month_winter = [9, 10, 11, 12, 1, 2, 3, 4, 5]
tomorrow = datetime.today() + timedelta(days=1)
tomorrow_iso = tomorrow.date().isoformat()
#param_dict_keys = ["temperature", "radiation", "windspeed", "daylight_hours"]
param_dict_keys = ["temperature", "windspeed", "daylight_hours"]


@typechecked
def eval_model(weather_day: WeatherDayModel, summer_season: bool = False) -> int:
    """
    param_dict: {"temperature": <C>, "radiation": <W/m2> ,"windspeed": <km/h>, "daylight_hours": <h>}
    return: kWh
    """
    #print(f"param_dict: {param_dict}")
    # normalization
    param_dict = weather_day.model_dump()
    #normalized_dict = {}
    kwh = model_dict["const"] # const value of model
    for field in param_dict_keys:
        #normalized_dict[field] = (param_dict[field] - model_dict["min"][field]) / (model_dict["max"][field] - model_dict["min"][field])
        normalized = (param_dict[field] - model_dict["min"][field]) / (model_dict["max"][field] - model_dict["min"][field])
        kwh = kwh + model_dict[field] * normalized # add each coefficient to const value

    #print(f"kWh: {kwh}")
    kwh = int(kwh)

    kwh = 10 if summer_season else kwh # Annahme: Warmwasser und Keller benötigen 12 kWh pro Tag Winter wie Sommer
    return kwh

if __name__ == "__main__":

    #from strom.util.weather import request_weather_data, get_weather_data
    from strom.util.weather import Weather

    today = date.today()
    tomorrow = today + timedelta(days=1)

    weather = Weather()
    weather.load(today, tomorrow, None)
    #data = weather.get_weather_data(today)
    #logger.debug(f"data of today ({today.isoformat()}):\n{data}")
    data = weather.get_weather_data(tomorrow)
    print(f"data of tomorrow ({tomorrow.isoformat()}):\n{data}")

    #tomorrow_dict = get_weather_data(request_weather_data(tomorrow, tomorrow), tomorrow).model_dump()
    tomorrow_dict = data.model_dump()
    tomorrow_dict["day"] = tomorrow

    #print(f"\ntomorrow")

    data = [
    ("tomorrow", tomorrow_dict),
    ("-40 Grad ohne Wind mit Sonne", {'day': tomorrow, 'radiation': 80, 'windspeed': 0, 'temperature': -40, 'daylight_hours': 7.00}),
    ("-10 Grad mit Wind ohne Sonne", {'day': tomorrow, 'radiation': 0, 'windspeed': 30, 'temperature': -10, 'daylight_hours': 7.00}),
    ("-10 Grad ohne Wind mit Sonne", {'day': tomorrow, 'radiation': 80, 'windspeed': 0, 'temperature': -10, 'daylight_hours': 7.00}),
    ("0 Grad ohne Wind ohne Sonne", {'day': tomorrow, 'radiation': 0, 'windspeed': 0, 'temperature': 0, 'daylight_hours': 10.00}),
    ("0 Grad mit Wind ohne Sonne", {'day': tomorrow, 'radiation': 0, 'windspeed': 30, 'temperature': 0, 'daylight_hours': 10.00}),
    ("0 Grad ohne Wind mit Sonne", {'day': tomorrow, 'radiation': 80, 'windspeed': 0, 'temperature': 0, 'daylight_hours': 10.00}),
    ("10 Grad mit Wind ohne Sonne", {'day': tomorrow, 'radiation': 20, 'windspeed': 30, 'temperature': 10, 'daylight_hours': 8.45}),
    ("10 Grad ohne Wind mit Sonne", {'day': tomorrow, 'radiation': 80, 'windspeed': 0, 'temperature': 10, 'daylight_hours': 8.45}),
    ("15 Grad mit Wind ohne Sonne", {'day': tomorrow, 'radiation': 20, 'windspeed': 30, 'temperature': 15, 'daylight_hours': 8.45}),
    ("15 Grad ohne Wind mit Sonne", {'day': tomorrow, 'radiation': 80, 'windspeed': 0, 'temperature': 15, 'daylight_hours': 8.45}),
    ("25 Grad ohne Wind mit Sonne", {'day': tomorrow, 'radiation': 80, 'windspeed': 0, 'temperature': 25, 'daylight_hours': 12.00})
    ]

    for d in data:
        weather_day = WeatherDayModel.model_validate(d[1])
        kwh = eval_model(weather_day)
        print(d[0])
        print(f"weather_day: {weather_day} --> {kwh} kWh")

    '''
    consumption  date_from    date_to heating_period  diff_days  temperature  radiation  windspeed  daylight_hours
    1060.2 2019-10-12 2019-10-31              w       19.0       5273.6    43026.0    1031.47      202.001389
    3048.6 2019-10-31 2019-12-06              w       36.0       5183.8    40773.0    3410.63      333.302778
     763.3 2019-12-06 2019-12-13              w        7.0        151.1     7321.0     445.50       59.475833
    4662.2 2019-12-13 2020-01-29              w       47.0       1829.5    49766.0    3307.56      404.069722
    '''
    #print()
    #for x in [
    #        {'consumption': 1060, 'diff_days': 19, 'radiation': 43026, 'windspeed': 1031, 'temperature': 5273, 'daylight_hours': 202},
    #        {'consumption': 3048, 'diff_days': 36, 'radiation': 40773, 'windspeed': 3410, 'temperature': 5183, 'daylight_hours': 333},
    #        {'consumption': 763,  'diff_days': 7,  'radiation': 7321,  'windspeed': 445,  'temperature': 151,  'daylight_hours': 59},
    #        {'consumption': 4662, 'diff_days': 47, 'radiation': 49766, 'windspeed': 3307, 'temperature': 1829, 'daylight_hours': 404}]:
    #    diff_days = x["diff_days"]
    #    y = {k: int(v / diff_days) for k, v in x.items()}
    #    print(y)
    #    ,y)
    #    print()
