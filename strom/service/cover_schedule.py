#
# Copyright (c) 2024 Alexander Exner.
#
# This file is part of strom
# (see https://gitlab.com/exner.occupational/strom).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from loguru import logger
from typing import List, Dict, Tuple, Union, Type
from datetime import datetime, timedelta
#from flask_table import Table, Col, DatetimeCol
import urllib.parse
from typing import TypeVar, Generic
from enum import Enum
from typeguard import typechecked
from strom.util.const import VIENNA #, UTC
from strom.util.epoch import MyDatetime
#from strom.service.control import Main, Boiler, Vent #, ColorizedEnum

# 
# Wohnzimmer:
# 
# wochentags 06:00 -> Auf
# wochentags 07:30 -> Zu
# wochentags 14:00 -> Auf
# WE 07:00 -> Auf
# Sonnenuntergang -> Zu
# 
# Schlafzimmer:
# 
# wochentags 16:00 -> Auf
# Sonnenuntergang -> Zu


class Cover(Enum):
    WZ_DOOR = 1
    WZ_WIN = 2
    SZ_DOOR = 3

class CoverPos(Enum):
    CLOSE = 1
    OPEN = 2
    SLIT = 3

# Define the object with datetime and enum attributes
@typechecked
class CoverKey:
    def __init__(self, dt: datetime, cover):
        self.dt = dt
        self.cover = cover

    def __hash__(self):
        return hash((self.dt, self.cover))

    def __eq__(self, other):
        return isinstance(other, CoverKey) and self.dt == other.dt and self.cover == other.cover

    def __lt__(self, other):
        if self.dt == other.dt:
            return self.cover.value < other.cover.value
        return self.dt < other.dt

    def get_my_datetime(self) -> MyDatetime:
        dt = self.dt
        return MyDatetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, tzinfo=dt.tzinfo)

    def __str__(self) -> str:
        return f"{self.dt.isoformat()} / {self.cover.name}"

@typechecked
class CoverSchedule():
    """
    Zur Steuerung, wann im Tagesverlauf Nachtstrom oder Tagstrom oder kein Strom bezogen werden soll.
    """
    def __init__(self):
        self.actions: Dict[CoverKey, int | CoverPos] = {}

    def del_actions_before(self, start: datetime) -> None:
        """
        Delete all actions before start

        :start: All actions before this datetime will be deleted
        """
        na = len(self.actions)
        self.actions = {cover_key: v for cover_key, v in self.actions.items() if cover_key.dt >= start}
        nb = len(self.actions)
        logger.debug(f"deleted {na - nb} actions")

    def del_action(self, dt: datetime, cover: Cover) -> None:
        """
        Delete one action

        :start: datetime of the action to delete
        """
        key = CoverKey(dt, cover)
        del self.actions[key]

    def add_action(self, dt: datetime, cover: Cover, action: int | CoverPos) -> None:
        """
        Add one action, sort actions afterwards

        :dt: datetime, time of the action
        :cover: cover to drive
        :action: open, close, slit mode or percentage to open
        """
        #logger.debug(f"dt: {dt} / duration: {duration} / self: {str(self)}")
        #logger.debug(f"dt: {dt} / duration: {duration}")
        #logger.debug(f"# overlapping starts / ends: {len(start_keys)} / {len(end_keys)}")

        key = CoverKey(dt, cover)
        self.actions[key] = action

        sorted_dict = dict(sorted(self.actions.items()))
        self.actions = sorted_dict

    def get_action(self, dt: datetime, cover: Cover) -> int | CoverPos | None:
        """
        Gibt den SOLL-Wert der Rollladen-Stellung zurück.

        :return: SOLL-Wert wenn vorhanden, sonst None.
        """
        key = CoverKey(dt, cover)
        return self.actions.get(key)

    def get_my_actions(self) -> Dict[CoverKey, str]:
       return {cover_key: action.name if isinstance(action, CoverPos) else f"{str(action)}%" for cover_key, action in self.actions.items()}

    def get_schedule_enum(self) -> type(Enum):
        return Cover

    def get_schedule_name(self) -> str:
        return Cover.__name__

    def get_action_names(self) -> List[str]:
        return [action.name for action in CoverPos]

    #def __str__(self) -> str:
    #    return f"Type: {self.t.__name__}\n" + "\n".join([f"{k.isoformat()} - {(k + v.duration).isoformat()}: {v.action}" for k, v in self.actions.items()])

cover_schedule = CoverSchedule()
