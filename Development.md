# Environments

```bash
# Switch to Development Environment
vst

# Switch to Service Environment
vse

# Create Environment
python -m venv ~/virtualenvs/<environment name>
```

# Swapping
```bash
# Swapping stoppen
sudo systemctl stop dphys-swapfile

# Swapping starten
sudo systemctl start dphys-swapfile
```

# Build wheel

```bash
# 1) Use development environment
vst
# 2) Increase version in setup.py
vim ~/git/strom/strom/version.py
# 3) Build wheel package
bld
# 4) Stop service
sto
# 5) Switch to service env, install latest wheel, switch back to development env
i
# 6) Start service
sta
```

# From Remote

# 1) update git repository
ssh pi@strom "cd git/strom && git pull"

# 1b) install packages
ssh pi@strom ". ~/virtualenvs/strom/bin/activate && cd ~/git/strom && pip install --editable .[test]"

# 2) 
ssh pi@strom '. ~/virtualenvs/strom/bin/activate && cd ~/git/strom && python setup.py bdist_wheel && sudo systemctl stop strom && . ~/virtualenvs/service/bin/activate && pip install dist/$(ls ~/git/strom/dist |sort |tail -1) && sudo systemctl start strom

# Configure as Service

```bash
# see: https://domoticproject.com/creating-raspberry-pi-service/
sudo cp /home/pi/git/strom/scripts/strom.service /lib/systemd/system/
sudo systemctl daemon-reload

# test
sudo systemctl restart strom

journalctl -f -u strom

# command to start automatically on reboot 
sudo systemctl enable strom

# From now on, you can check easier the status of the service by doing:
sudo service strom status/start/stop
```

# Code Analysis

# pyflakes
```
pip install pyflakes
find . -name "*.py" | xargs pyflakes
```
