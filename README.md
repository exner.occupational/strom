# strom

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/exner.occupational/strom.git
git branch -M main
git push -uf origin main
```

# Data

## Historical "aWATTar GmbH" Pricing
```
curl "https://api.awattar.at/v1/marketdata?start=<epoch incl milis>"
```
First data available as of: 01.01.2014

## Geo Location

Source Google Maps (latitude, longitude): 48.17425384658693, 16.255988490068848

## Historical Weather

Source: https://open-meteo.com/en/docs/historical-weather-api#

### Parameters

latitude=48.1742
longitude=16.2559
start_date=2014-01-01
end_date=2022-12-26
hourly=temperature_2m,direct_radiation,windspeed_10m
timezone=Europe/Berlin
windspeed_unit=ms

URL: https://open-meteo.com/en/docs/historical-weather-api#latitude=48.1742&longitude=16.2559&start_date=2014-01-01&end_date=2022-12-26&hourly=temperature_2m,direct_radiation,windspeed_10m&timezone=Europe%2FBerlin&windspeed_unit=ms

### Download

./data/historical_weather_2014_2022.csv

## Abrechnungen Wien Energie

* von
* bis
* kWh
* Gesamtpreis
* Rabatt
* Verbrauchspreis (Unterperioden)
  - von
  - bis
  - kWh
  - Preis

### Mess-Historie anreichern

Unser Excel (Dokumente/Haus/Strom_Elektrik/Zählerstände_Strom_2022_09_22.ods) mit den "Z-Werten" (Zählerablesung durch Wiener Netze) der Abrechnungen anreichern.

### Wiener Netze Verbrauchsdaten
https://github.com/platysma/vienna-smartmeter
https://github.com/florianL21/vienna-smartmeter

### Vergleich mit aWATTar

aWATTar-Daten in gleicher Periode wie Wien Energie aufsummieren und vergleichen

## Energieverbrauchs-Formel

### Einfach

Pro Periode in unserem Excel:

```
# Energie pro Stunde und Temperatur-Differenz
e = kWH / Summe von (18 Grad - Temperatur lt. historischen Wetter-Daten)
```

### Lineare Regression

```
e = b1 * Temperatur + b2 * Einstrahlung + b3 * Taglänge + b4 * Windgeschwindigkeit + a

# pro Periode
e ... kWh
Temperatur ... Summe aller temperature_2m (°C)
Einstrahlung ... Summe aller direct_radiation (W/m²)
Windgeschwindigkeit ... Summe aller windspeed_10m (m/s)
Einstrahlung ... Summe aller (sunset (iso8601) - sunrise (iso8601))
```

### Shelly

https://shelly-api-docs.shelly.cloud/gen2/ComponentsAndServices/Cover/
